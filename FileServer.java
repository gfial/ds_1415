

import java.net.MalformedURLException;
import java.rmi.*;

public interface FileServer extends Remote {

	public static final int HEARTBEAT_INTERVAL = 2000;
	
	/**
	 * Lists a directory from the server.
	 * @param path - String with the path pointing to the directory to list.
	 * @return String array of directories/files contained in the path.
	 * @throws RemoteException
	 * @throws InfoNotFoundException
	 */
	public String[] ls(String path) throws RemoteException, InfoNotFoundException;
	
	/**
	 * Creates a directory in the server.<br/>
	 * Secondary servers will only perform the operation if its coming from a primary server.
	 * @param from - String with the address of the client calling the operation.
	 * @param dirname - String with the name of the directory to be created.
	 * @return Returns a boolean indicating whether the operation was successful.
	 * @throws RemoteException
	 * @throws InfoNotFoundException
	 * @throws MalformedURLException
	 * @throws NotBoundException
	 */
	public boolean mkdir(String from, String dirname) throws RemoteException, InfoNotFoundException, MalformedURLException, NotBoundException;
	
	/**
	 * Creates a directory in the server.<br/>
	 * Only executes if the server is primary.
	 * Assumes that the caller is not a primary server.<br/>
	 * See {@link FileServer#mkdir(String, String)} if trying to call on secondaries.
	 * @param dirname - String with the name of the directory to be created.
	 * @return Returns a boolean indicating whether the operation was successful.
	 * @throws RemoteException
	 * @throws InfoNotFoundException
	 * @throws MalformedURLException
	 * @throws NotBoundException
	 */
	public boolean mkdir(String dirname) throws RemoteException, InfoNotFoundException, MalformedURLException, NotBoundException;
	
	/**
	 * Remove an empty directory in the server.<br/>
	 * If the directory is not empty, operation is not executed.
	 * @param from - String with the address of the client calling the operation.
	 * @param dirname - String with the name of the directory to be removed.
	 * @return Returns a boolean indicating whether the operation was successful.
	 * @throws RemoteException
	 * @throws InfoNotFoundException
	 * @throws NotBoundException 
	 * @throws MalformedURLException 
	 */
	public boolean rmdir(String from, String dirname) throws RemoteException, InfoNotFoundException, MalformedURLException, NotBoundException;
	
	/**
	 * Remove an empty directory in the server.<br/>
	 * If the directory is not empty, operation is not executed.<br/>
	 * Only executes if the server is primary.<br/>
	 * Assumes that the caller is not a primary server.<br/>
	 * See {@link FileServer#rmdir(String, String)} if trying to call on secondaries.
	 * @param from - String with the address of the client calling the operation.
	 * @param dirname - String with the name of the directory to be removed.
	 * @return Returns a boolean indicating whether the operation was successful.
	 * @throws RemoteException
	 * @throws InfoNotFoundException
	 * @throws NotBoundException 
	 * @throws MalformedURLException 
	 */
	public boolean rmdir(String dirname) throws RemoteException, InfoNotFoundException, MalformedURLException, NotBoundException;
	
	/**
	 * Removes a file in the server.
	 * @param from - String with the address of the client calling the operation.
	 * @param path - String containg the path that leads to the file to be deleted.
	 * @return Returns a boolean indicating whether the operation was successful.
	 * @throws RemoteException
	 * @throws InfoNotFoundException
	 * @throws NotBoundException 
	 * @throws MalformedURLException 
	 */
	public boolean rm(String from, String path) throws RemoteException, InfoNotFoundException, MalformedURLException, NotBoundException;
	
	/**
	 * Removes a file in the server.<br/>
	 * Only executes if the server is primary.<br/>
	 * Assumes that the caller is not a primary server.<br/>
	 * See {@link FileServer#rm(String, String)} if trying to call on secondaries.
	 * @param from - String with the address of the client calling the operation.
	 * @param path - String containg the path that leads to the file to be deleted.
	 * @return Returns a boolean indicating whether the operation was successful.
	 * @throws RemoteException
	 * @throws InfoNotFoundException
	 * @throws NotBoundException 
	 * @throws MalformedURLException 
	 */
	public boolean rm(String path) throws RemoteException, InfoNotFoundException, MalformedURLException, NotBoundException;
	
	/**
	 * Provides information about the file/directory localed at the given path.
	 * @param path - The path in the server where the file/directory is located.
	 * @return A {@link FileInfo} object containing the information about the file/directory.
	 * @throws RemoteException When the server is unreachable.
	 * @throws InfoNotFoundException When the path leads to no file/directory.
	 */
	public FileInfo getattr(String path) throws RemoteException, InfoNotFoundException;

	/**
	 * Gets a file from the server in which the remote method is implemented.
	 * @param fromPath - Path in the server's directory that leads to the desired file.
	 * @return Byte array with the file coming from the server.
	 * @throws RemoteException
	 * @throws InfoNotFoundException
	 */
	public byte[] downloadFileFromServer(String fromPath) throws RemoteException, InfoNotFoundException;

	/**
	 * Copies a file from the client (whoever calls the method) into the server
	 *  (where the method is implemented).
	 * @param from - String with the address of the sender (client).
	 * @param file - Byte array containing the file to be sent.
	 * @param filename - Name of the file.
	 * @param toPath - Path on the server to which the file should be saved.
	 * @return Boolean indicating whether the operations was successful or not.
	 * @throws RemoteException
	 * @throws InfoNotFoundException
	 * @throws NotBoundException 
	 * @throws MalformedURLException 
	 */
	public boolean uploadFileToServer(String from, byte[] file, String filename, String toPath) throws RemoteException, InfoNotFoundException, MalformedURLException, NotBoundException;
	
	/**
	 * Copies a file from the client (whoever calls the method) into the server
	 *  (where the method is implemented).<br/>
	 * Only executes if the server is primary.<br/>
	 * Assumes that the caller is not a primary server.<br/>
	 * See {@link FileServer#uploadFileToServer(String, byte[], String, String)} if trying to call on secondaries.
	 * @param from - String with the address of the sender (client).
	 * @param file - Byte array containing the file to be sent.
	 * @param filename - Name of the file.
	 * @param toPath - Path on the server to which the file should be saved.
	 * @return Boolean indicating whether the operations was successful or not.
	 * @throws RemoteException
	 * @throws InfoNotFoundException
	 * @throws NotBoundException 
	 * @throws MalformedURLException 
	 */
	public boolean uploadFileToServer(byte[] file, String filename, String toPath) throws RemoteException, InfoNotFoundException, MalformedURLException, NotBoundException;
	
}
