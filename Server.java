
import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.List;


@SuppressWarnings("deprecation")
public abstract class Server extends UnicastRemoteObject implements FileServerConfiguration{

	protected static final long serialVersionUID = 1L;
	protected File basePath;
	protected String nameId, primary, self;
	protected List<String> secondaries;
	protected static String csURL;
	protected boolean isPrimary;
	protected static String USE_INSTRUCTIONS;
	protected static String csaddr;
	protected static String path;
	protected static String name;
	

	protected Server() throws IOException, NotBoundException {
		super();	
	}

	public synchronized void makePrimary(String selfAddress, String[] secondaries) {
		
		isPrimary = true;
		primary = selfAddress;
		self = selfAddress;
		this.secondaries.clear();
		
		System.out.print("\nServer is now PRIMARY");
		
		if(secondaries == null)
			System.out.println(" with no secondaries");
		
		else {
			for(int i = 0; i < secondaries.length; i++) {
				if(!secondaries[i].equals(primary))
					this.secondaries.add(secondaries[i]);
			}
			
			if(this.secondaries.isEmpty())
				System.out.println(" with no secondaries");
			
			System.out.println(" with:");
			
			for(int i = 0; i < secondaries.length; i++) {
				if(!secondaries[i].equals(primary))
					System.out.println("\t" + secondaries[i] + " as SECONDARY");
			}
		}
	}
 
	public void makeSecondary(String selfAddress, String primary) {
		
		secondaries.clear();
		self = selfAddress;
		isPrimary = false;
		this.primary = primary;
		System.out.println("\nServer is now SECONDARY with " + primary + " as PRIMARY");
		
		try {
			FileServerConfiguration primaryStub = (FileServerConfiguration) Naming.lookup(primary);
			rmrec(null);
			if(!primaryStub.requestReplication(self))
				System.out.println("makeSecondary: Could not request replication.");
		} catch (Exception e) {
			System.out.println("makeSecondary: Unable to contact primary, disconnecting.");
			//TODO: Disconnect from contact server.
		}
	}
	
	public boolean requestReplication(String server) throws RemoteException {
		
		System.out.println("Replicating to " + server);
		if(!secondaries.contains(server))
			return false;
		try {
			if(replicateToServer(null, (FileServer) Naming.lookup(server)))
				return true;
		} catch (Exception e) {
			secondaries.remove(server);
			return false;
		}
		
		return false;
	}
	
	/**
	 * Replicates this primary server into a secondary server.
	 * @param path - From which directory to start replicating, should start at <i>null</i> (default).
	 * @param fs - The stub to the secondary server.
	 * @return If the operation was sucessful.
	 */
	private boolean replicateToServer(String path, FileServer fs) {
		
		if(path == null)
			path = ".";
		try{
			String[] serverfiles = ls(path);
			String filepath;
			for(int i = 0; i < serverfiles.length; i++) {
				
			if(path == ".")
				filepath = serverfiles[i];
			else
				filepath =  path + "/" + serverfiles[i];
				
				FileInfo file = getattr(filepath);
				
				if(file == null) {
					System.out.println("Replication: "+filepath+" not returning information.");
					return false;
				}
				
				// If file to be synced is a normal file
				if(file.isFile) {
					if(!sendFileToServer(serverfiles[i], path, fs)) {
						System.out.println("Replication: Error copying " + filepath + " to secondary.");
						return false;
					}
				}
				
				// If file to be synced is a directory
				else {
					
					try {
						fs.mkdir(primary, filepath);
					} catch (Exception e) {
						System.out.println("Replication: Error making remote directory: " + path+"/"+serverfiles[i]);
						return false;
					}
					
					if(!replicateToServer(filepath, fs))
						return false;
				}
					
			}
		} catch(Exception e) {
			System.out.println("Replication: Error");
			return false;
		}
		
		return true;
	}
	
	/**
	 * Recursively cleans a directory.
	 * @param path - The path poiting to the directory to be cleaned. Leave <i>null</i> for default (clean root).
	 * @return If the operation was sucessful.
	 */
	private boolean rmrec(String path) {
		
		if(path == null)
			path = ".";
		
		try{
			String[] files = ls(path);
			
			for(int i = 0; i < files.length; i++) {
				
				String filepath = path + "/" + files[i];
				FileInfo fileattr = getattr(filepath);
				
				if(fileattr == null) {
					System.out.println("rmrec: "+filepath+" not returning information.");
					return false;
				}
				
				// If file to be removed is a normal file
				if(fileattr.isFile) {

					if(!rm(self, filepath)) { 
						System.out.println("rmrec: Error deleting " + filepath);
						return false;
					}
				}
				
				// If file to be synced is a directory
				else {
					if(!rmrec(filepath)) {
						System.out.println("rmrec: Error deleting contents of directory: " + filepath);
						return false;
					}

					if(!rmdir(self, filepath)) {
						System.out.println("rmrec: Error deleting directory: " + filepath);
						return false;
					}
				}
					
			}
		} catch(Exception e) {
			System.out.println("rmrec: Exception");
			return false;
		}
		
		return true;
	}
	
	/**
	 * Copies a file into another server.
	 * @param filename - Name of the file to copy.
	 * @param parentdirpath - Path without the file, if we want to copy <i>./dir/file.txt</i>, put <i>./dir</i> in this field.
	 * @param toServer - Server stub where to copy the file.
	 * @return If the operation was sucessful.
	 */
	private boolean sendFileToServer(String filename, String parentdirpath, FileServer toServer) {
		String path;
		if(parentdirpath==".")
			path = filename;
		else 
			path = parentdirpath + "/" + filename;
		try{
			byte[] file = downloadFileFromServer(path);
			
			if (file == null)
				return false;
			
			return toServer.uploadFileToServer(primary, file, filename, parentdirpath);
		
		}catch (Exception e) {
			return false;
		}
		
		
	}

	/**
	 * Find a name ID for the given server with a given name, if a server is already running<br/>
	 * in the same machine then any started servers on the same machine must have different <br/>
	 * names, this method creates a new name for a new server so it may bound itself.<br/>
	 * For example:<br/>Start server named SD (first), this method provides "SD_0".<br/>
	 * Start a second server named SD, this method will provide "SD_1", and so on...
	 * @param name - The name of the server
	 * @param fs - The server's stub
	 * @return - The name ID for the given name and server pair.
	 * @throws MalformedURLException
	 * @throws RemoteException
	 */
	private static String registerFileServer(String name, FileServer fs) throws MalformedURLException, RemoteException {
		
		String serverName = name + "_0";
		boolean serverUnset = true;
		
		for(int i = 0; serverUnset == true; i++) {
			
			serverName = name + "_" + i;
			
			// Find a registry name id available for the server based on the server name...
			try {
				Naming.bind( "/" + serverName, fs);
				System.out.println( serverName + " bound in registry.");
				serverUnset = false;
				
			} catch( AlreadyBoundException e) {
				// Try different server name
			}
		}
		
		return serverName;
	}
	
	private static void establishCSConnection(String name, String nameId) throws IOException, NotBoundException {

		ContactServer cs = null;
		String receivedString, csRequest;
		InetAddress csAddr, group;
		int sentPackets = 0, hbPort;
		boolean keepSending = true;
		
		group = InetAddress.getByName(ContactServer.MULTICAST_GROUP);
		csAddr = null;
		receivedString = null;
			
		System.out.println("Searching contact server in local network...");
	
		byte[] buf = new byte[1024];
		DatagramSocket socket = new DatagramSocket();
		DatagramPacket connection_request, received_packet, heartbeat;
		
		// Simple datagram socket seems to be working in multicast... good practice?
		csRequest = name + " " + nameId;
		
		received_packet = new DatagramPacket(buf, buf.length);
		socket.setSoTimeout(4000);
		
		if(csURL != null){
			String s = csURL.substring(csURL.indexOf("/")+2, csURL.lastIndexOf("/"));
			try{
				cs = (ContactServer) Naming.lookup(csURL);
				csAddr =InetAddress.getByName(s);
				connection_request = new DatagramPacket(csRequest.getBytes(), csRequest.length(), csAddr, ContactServer.MULTICAST_SERVER_PORT);
				socket.send(connection_request);
				socket.receive(received_packet);
				keepSending = false;
				System.out.println("Contact server found in "+csURL);
			}catch (Exception e){
				System.out.println("No contact server found at "+csURL+" .");
			}
		}
		
		connection_request = new DatagramPacket(csRequest.getBytes(), csRequest.length(), group, ContactServer.MULTICAST_SERVER_PORT);
		// Send a request to connect with the contact server in a multicast address
		
		while (keepSending) {
			try {
				socket.send(connection_request);
				sentPackets++;
				System.out.print(".");
				socket.receive(received_packet);
				receivedString = new String(received_packet.getData(), 0, received_packet.getLength());
				csAddr = received_packet.getAddress();
				System.out.println("\nReceived \"" + receivedString + "\" from " + received_packet.getAddress() + ":" + received_packet.getPort());
				cs = (ContactServer) Naming.lookup(receivedString);
				keepSending = false;
				System.out.println("Contact server found (" + receivedString + ")");
				
			} catch (SocketTimeoutException e2) {
				// keep sending
			} catch (Exception e) {
				try {
					e.printStackTrace(System.err);
					Thread.sleep(HEARTBEAT_INTERVAL);
				} catch (InterruptedException ie) {
				ie.printStackTrace();
				}
			}

		}		
		
		String msg;
		hbPort = cs.getHeartbeatPort();
		
		// Maintain connection through the use of heartbeats
		msg = ContactServer.FILESERVER_HEARTBEAT + " " + nameId;
		heartbeat = new DatagramPacket(msg.getBytes(), msg.length(), csAddr, hbPort);
		
		socket.setSoTimeout(HEARTBEAT_INTERVAL);
		keepSending = true;
		buf = new byte[8];
		sentPackets = 0;
		received_packet = new DatagramPacket(buf, buf.length);
		boolean response = true;
		
		while(keepSending) {
			
			socket.send(heartbeat);
			sentPackets++;
			
			try {
				
				socket.receive(received_packet);
				response = true;
				receivedString = new String(received_packet.getData(), 0, received_packet.getLength());
				
				if(receivedString.equalsIgnoreCase(ContactServer.FILESERVER_ACK))
					sentPackets = 0;
				
			} catch(SocketTimeoutException e) {
				// Keep sending...
				response = false;
			}
			
			if(response) {
				try {
					Thread.sleep(HEARTBEAT_INTERVAL);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			if(sentPackets * HEARTBEAT_INTERVAL > ContactServer.MAX_NONRESPONDING_TIME) {
				System.out.println("Lost connection to server...");
				break;
			}
		}
		
		socket.close();
		
	}

	protected void begin(String name, String path, String csaddr) throws RemoteException, IOException{
		isPrimary = false;
		csURL = csaddr;
		secondaries = new LinkedList<String>();
		
		try { // start rmiregistry
			LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
		} catch( RemoteException e) { 
			// if not start it
			// do nothing - already started with rmiregistry
			LocateRegistry.getRegistry();
		}
		
		nameId = registerFileServer(name, this);
		
		// Create directory for the server to operate on
		if(path == null) {
			basePath = new File(".");
			new File(basePath, nameId).mkdir();
			basePath = new File(nameId);	
		}
		
		else
			basePath = new File(path);
		
		while(true) {
			
			try {
				establishCSConnection(name, nameId);
				
			} catch(RemoteException e) {
				// Go search for contact server again
			} catch(Exception e) {
				e.printStackTrace(System.err);
			} finally {
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace(System.err);
				}
			}
		}
	}
	
	protected static void getStarted(String[] args){

				// args: <nome server> <url contact server>(optional)
			switch(args.length){
			case 3:
				csaddr = args[2]; //aqui n�o leva break de proposito, para executar o case 2 tambem
			case 2:
				path = args[1];//aqui n�o leva break de proposito, para executar o case 1 tambem
			case 1:{
				name = args[0];
				break;
			}
			default:
				break;
			}

			try {
				System.getProperties().put( "java.security.policy", "src/policy.all");
				if( System.getSecurityManager() == null) {
					System.setSecurityManager( new RMISecurityManager());
				}
				
					
			} catch( Throwable th) {
				th.printStackTrace();
			}
		}
}
