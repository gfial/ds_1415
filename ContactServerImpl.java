import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Timestamp;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Iterator;
import java.util.Map.Entry;

@SuppressWarnings("deprecation")
public class ContactServerImpl extends UnicastRemoteObject implements ContactServer {
	
	private ConcurrentHashMap<String, ConcurrentHashMap<String, ServerInfo>> serverMap;
	private ConcurrentHashMap<String, String> primaryServers;
	private static final long serialVersionUID = 1L;
	private int heartbeatPort;
	
	Registry reg;
	
	public ContactServerImpl() throws Exception {
		
		serverMap = new ConcurrentHashMap<String, ConcurrentHashMap<String, ServerInfo>>();
		primaryServers = new ConcurrentHashMap<String, String>();
		
		try { // start rmiregistry
			reg = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
		} catch( RemoteException e) { 
			// if not start it
			// do nothing - already started with rmiregistry
			reg = LocateRegistry.getRegistry();
		}
		
		Naming.rebind( "/" + CONTACT_SERVER_NAME, this);
		System.out.println( CONTACT_SERVER_NAME + " bound in registry (port: " + Registry.REGISTRY_PORT + ")");
		
		ContactThread cThread = new ContactThread();
		HeartbeatThread hThread = new HeartbeatThread();
		ServerStatusThread sThread = new ServerStatusThread();
		
		cThread.start();
		hThread.start();
		sThread.start();
		
		heartbeatPort = hThread.port;
		
	}
	
	public boolean registerServer(String name, String srvUrl) throws RemoteException {
		
		FileServerConfiguration fs;
		boolean primary = false;
		
		if(name.isEmpty() || srvUrl.isEmpty())
			return false;
		
		try {
			fs = (FileServerConfiguration) Naming.lookup(srvUrl);
			
		} catch(Exception e) {
			System.out.println(srvUrl + " is invalid RMI address...");
			return false;
		}
		
		if(!serverMap.containsKey(name)) {
			primary = true;
			serverMap.put(name, new ConcurrentHashMap<String, ServerInfo>());
		}
		
		if(!serverMap.get(name).containsKey(srvUrl)) {
			ServerInfo srvInfo =  new ServerInfo(name, srvUrl, new Timestamp(System.currentTimeMillis()));
			serverMap.get(name).put(srvUrl, srvInfo);
			
			if(primary) {
				primaryServers.put(name, srvUrl);
				fs.makePrimary(srvUrl, null);
				System.out.println("Registered PRIMARY " + serverMap.get(name).get(srvUrl) + ".");
			}
			else {
				FileServerConfiguration pfs;
				try {
					pfs = (FileServerConfiguration) Naming.lookup(primaryServers.get(name));
				} catch(Exception e) {
					System.out.println("PRIMARY server failed while registering a secondary...");
					removePrimaryServer(name);
					System.out.println("Registered " + serverMap.get(name).get(srvUrl) + ".");
					return false;
				}
				
				pfs.makePrimary(this.getPrimaryServer(name), getSecondaryServers(name));
				fs.makeSecondary(srvUrl, this.getPrimaryServer(name));
				
				System.out.println("Registered " + serverMap.get(name).get(srvUrl) + ".");
			}
		}
		
		return true;
	}
	
	public int getHeartbeatPort() throws RemoteException {
		return heartbeatPort;
	}

	public String[] listServers(String name) {
		
		String srvList[];
		int aux = 1;
		
		if(name != null) {
			
			//String primaryServer = "";
			ConcurrentHashMap<String, ServerInfo> entry = serverMap.get(name);
			
			if(entry == null)
				return null;
			
			//if(primaryServers.containsKey(name))
				//primaryServer = primaryServers.get(name);
			
			srvList = new String[entry.size()];
			Iterator<Entry<String, ServerInfo>> it = serverMap.get(name).entrySet().iterator();
			ServerInfo srv;
			
			for(int i = 0; it.hasNext(); i++) {
				
				srv = it.next().getValue();
				
				// if server being listed is a primary server
				
//				if(primaryServer.equals(srv.getUrl()))
//					srvList[i] = srv.toString() + " (PRIMARY)";
//					
//				else
				//TODO : isto nao pode estar aqui porque eh necessaria lista de url de servidores
				//quando muito por isso no lado do cliente, para apresentacao
					srvList[i] = srv.toString();
			}
		}
		
		else {
			
			Iterator<Entry<String, ConcurrentHashMap<String, ServerInfo>>> it = serverMap.entrySet().iterator();
			Entry<String, ConcurrentHashMap<String, ServerInfo>> serverEntry;
			
			srvList = new String[serverMap.size()];
			
			for(int i = 0; it.hasNext(); i++) {
				
				serverEntry = it.next();
				aux = (int) serverEntry.getValue().mappingCount();
				
				if(aux == 1)
					srvList[i] = serverEntry.getKey() + " (1 server)";
				else
					srvList[i] = serverEntry.getKey() + " (" + aux + " servers)";
			}
		}
		
		return srvList;
		
	}
	
	@Override
	public boolean isPrimary(String server) {
		
		Iterator<String> it = primaryServers.values().iterator();
		
		while(it.hasNext()) {
			
			String primaryServer = it.next();
			
			if(server.equals(primaryServer))
				return true;
		}
		
		return false;
	}

	public String getPrimaryServer(String name) {
		
		return primaryServers.get(name);
	}
	
	public String[] getSecondaryServers(String name) {
		
		if(!serverMap.containsKey(name) || serverMap.get(name).isEmpty())
			return null;
		
		// If only primary server exists
		if((serverMap.get(name).size() - 1) == 0)
			return null;
		
		String primary = primaryServers.get(name);
		String[] result;
		
		Iterator<ServerInfo> it = serverMap.get(name).values().iterator();
		
		// If no primary server exists
		if(primary == null) {
			result = new String[serverMap.get(name).size()];
			
			for(int i = 0; it.hasNext() && i < result.length; i++)
				result[i] = it.next().getUrl();
		}
		
		else {
			result = new String[serverMap.get(name).size() - 1];
			
			for (int i = 0; it.hasNext() && i < result.length; i++) {
				String current = it.next().getUrl();

				if (!current.equals(primary))
					result[i] = current;
				else
					i--;
			}
		}
		
		return result;
	}
	
	public static void main(String[] args) {
		
		try {
			
			System.getProperties().put( "java.security.policy", "src/policy.all");

			// ContactServer precisa de security na mesma?
			if( System.getSecurityManager() == null) {
				System.setSecurityManager( new RMISecurityManager());
			}

			new ContactServerImpl();
			
		} catch( Throwable th) {
			th.printStackTrace();
		}
		
	}
	
	/**
	 * This method assigns a new primary server given a server name.<br/>
	 * Changes server map in the contact server but also sets the servers again with the
	 * new configutation.
	 * @param name - The server name.
	 */
	private void assignNewPrimary(String name) {
		
		// No servers with that name available
		if(!serverMap.containsKey(name) || serverMap.get(name).isEmpty())
			return;
		
		String newPrimary;
		boolean reachable = false;
		
		do {
			newPrimary = serverMap.get(name).elements().nextElement().getUrl();
			
			if(newPrimary.equals(getPrimaryServer(name)))
				continue;
			
			try {
				((FileServerConfiguration) Naming.lookup(newPrimary)).makePrimary(newPrimary, getSecondaryServers(name));
				reachable = true;
			} catch(Exception e) {
				// Picked server is failing, discard it.
				serverMap.get(name).remove(newPrimary);
			}
		} while(!reachable && !serverMap.get(name).isEmpty());
		
		// If no reachable/working primary servers found
		if(serverMap.get(name).isEmpty()) {
			
			serverMap.remove(name);
			System.out.println("No reachable primary servers for " + name);
			return;
		}
				
		primaryServers.put(name, newPrimary);		
		System.out.println(primaryServers.get(name)+ " now PRIMARY for " + name);
		
		String[] secondaries = getSecondaryServers(name);
		
		// If primary is the only server of its name
		if(secondaries == null)
			return;
		
		// Inform secondaries about new primary server
		for(int i = 0; i < secondaries.length; i++) {
			
			try {
				((FileServerConfiguration) Naming.lookup(secondaries[i])).makeSecondary(secondaries[i], newPrimary);
			} catch(Exception e) {
				// Picked server is failing, discard it.
				serverMap.get(name).remove(secondaries[i]);
				System.out.println("Unable to tell " + secondaries[i] + " about new PRIMARY");
			}
		}
	}
	
	private void removePrimaryServer(String serverName) {
		
		if(serverName == null)
			return;
		
		String url = primaryServers.get(serverName);
		
		if(url != null) {
			primaryServers.remove(serverName);
			serverMap.get(serverName).remove(url);
		}
		
		assignNewPrimary(serverName);
	}
	
	/**
	 * This thread awaits for file servers/file clients and sends them the contact server RMI address.
	 * @author Guilherme
	 *
	 */
	private class ContactThread extends Thread {
		
		String url;
		String fsRequest[];
		MulticastSocket receiveSocket;
		DatagramSocket sendingSocket;
		DatagramPacket response;
		byte[] buf;
		DatagramPacket rcv;
		
		public ContactThread() throws IOException {
			
			InetAddress group = InetAddress.getByName(MULTICAST_GROUP);
			url = "//" + InetAddress.getLocalHost().getHostAddress() + "/" + CONTACT_SERVER_NAME;
			receiveSocket = new MulticastSocket(MULTICAST_SERVER_PORT);
			receiveSocket.joinGroup(group);
			sendingSocket = new DatagramSocket();
			System.out.println("Running as " + url + " (RMI)");
		}
		
		@Override
		public void run() {
			while(true) {
				try {
					buf = new byte[MAX_SERVER_NAME_LENGHT*2+ 10];
					rcv = new DatagramPacket(buf, buf.length);

					receiveSocket.receive(rcv);

					if(rcv.getLength()==0) //recebeu contacto de cliente
						System.out.println("ContactThread: Received Client contact");
					else
						System.out.println("ContactThread: Received [" + new String(rcv.getData(), 0, rcv.getLength()) + "]");

					if(rcv.getLength() > 0) {
						
						fsRequest = new String(rcv.getData(), 0, rcv.getLength()).split(" ");
						
						if(fsRequest.length == 2) {
							
							registerServer(fsRequest[0], "/" + rcv.getAddress() + "/" + fsRequest[1]);
						}
					}
					
					response = new DatagramPacket(url.getBytes(), url.length(), rcv.getAddress(), rcv.getPort());
					sendingSocket.send(response);
				} catch (IOException e) {
					System.out.print("Error establishing connection...");
					e.printStackTrace();
				}
			}
		}
		
	}
	
	/**
	 * This thread awaits heartbeats from file servers, if it gets them then it sends an acknowledge
	 * back and updates their timestamp.
	 * @author Guilherme
	 *
	 */
	private class HeartbeatThread extends Thread {
		
		DatagramSocket selfSocket;
		String[] rcv_message;
		String serverName, serverUrl;
		byte[] buf;
		DatagramPacket rcv;
		int port;
		
		public HeartbeatThread() throws IOException {
			
			selfSocket = new DatagramSocket();
			port = selfSocket.getLocalPort();
		}
		
		@Override
		public void run() {
			while(true) {
				try {
					buf = new byte[128];
					rcv = new DatagramPacket(buf, buf.length);
					selfSocket.receive(rcv);
					rcv_message = new String(rcv.getData(), 0, rcv.getLength()).split(" ");
					//System.out.println("HeartbeatThread: [" + new String(rcv.getData(), 0, rcv.getLength()) + "]");
					
					if(rcv_message[0].equalsIgnoreCase(FILESERVER_HEARTBEAT)) {
						
						serverName = rcv_message[1].substring(0, rcv_message[1].lastIndexOf('_'));
						
						if(!serverMap.containsKey(serverName))
							break;
						
						serverUrl = "/" + rcv.getAddress() + "/" + rcv_message[1];
						
						if(!serverMap.get(serverName).containsKey(serverUrl))
							break;
						
						Timestamp t = serverMap.get(serverName).get(serverUrl).getTimestamp();
						
						if(t != null) {
							
							t.setTime(System.currentTimeMillis());
							DatagramPacket response = new DatagramPacket(FILESERVER_ACK.getBytes(), FILESERVER_ACK.length(), rcv.getAddress(), rcv.getPort());
							selfSocket.send(response);
						}
						
					}
					
				} catch (IOException e) {
					System.out.print("Error establishing connection...");
					e.printStackTrace();
				} 
			}
		}
		
	}
	
	/**
	 * This thread keeps checking for file servers that are no longer responding.
	 * @author Guilherme
	 *
	 */
	private class ServerStatusThread extends Thread {
		
		public ServerStatusThread() {
			
		}
		
		@Override
		public void run() {
			
			while(true) {
				
				try {
					sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				Iterator<Entry<String,ConcurrentHashMap<String, ServerInfo>>> nameIt = serverMap.entrySet().iterator();
				Iterator<Entry<String, ServerInfo>> nameUrlIt;
				Entry<String, ConcurrentHashMap<String, ServerInfo>> nameEntry;
				Entry<String, ServerInfo> urlEntry;
				
				while(nameIt.hasNext()) {
					
					nameEntry = nameIt.next();
					nameUrlIt = nameEntry.getValue().entrySet().iterator();
					
					while(nameUrlIt.hasNext()) {
						
						urlEntry = nameUrlIt.next();
						
						// If a server timed out
						if(System.currentTimeMillis() - urlEntry.getValue().getTimestamp().getTime() > MAX_NONRESPONDING_TIME) {
							
							// If a primary server is timing out
							if(primaryServers.containsKey(nameEntry.getKey()) && primaryServers.get(nameEntry.getKey()).equals(urlEntry.getKey()))
								removePrimaryServer(nameEntry.getKey());
								
							// If secondary is timing out
							// TODO: Reset primary servers without this offline secondary
							else
								nameEntry.getValue().remove(urlEntry.getKey());
							
							// If there are no remaining servers under a name
							if(nameEntry.getValue().isEmpty())
								serverMap.remove(nameEntry.getKey());
							
						}
					}
				}
			}
		}
	}
}
