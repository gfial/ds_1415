import java.rmi.RemoteException;

public interface FileServerConfiguration extends FileServer {

	/**
	 * Configures the server to act as a primary server.
	 * @param address - The self address intended to indentify itself as primary to other servers.
	 * @param secondaries - List of secondary server addresses.
	 * @throws RemoteException If server becomes unreachable.
	 */
	public void makePrimary(String address, String[] secondaries) throws RemoteException;
	
	/**
	 * Configures the server to act as a secondary server.
	 * @param address - The self address intended to indentify itself to other servers.
	 * @param primary - The address of the primary server.
	 * @throws RemoteException If server becomes unreachable.
	 */
	public void makeSecondary(String address, String primary) throws RemoteException;
	
	/**
	 * Secondary servers call this method to have the contents of their primary server copied to them.
	 * @param fs - The URL of the server requesting the replication.
	 * @return If the operation was sucessful.
	 * @throws RemoteException If server becomes unreachable.
	 */
	public boolean requestReplication(String server) throws RemoteException;
	
}
