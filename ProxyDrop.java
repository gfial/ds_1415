
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.DropBoxApi;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;

public class ProxyDrop extends Server
{
	private static final long serialVersionUID = 1L;
	private static Token accessToken;
	private static String SCOPE = "dropbox"; 
	private static String API_KEY = "ig2ig0rq7tnnal6";
	private static String API_SECRET = "2dxe6hqiukiqbrp";
	private static String API_CODE_FILE = "src/dropB";
	private static OAuthService service;

	protected ProxyDrop(String name, String path, String csaddr) throws RemoteException, IOException, NotBoundException {
		super();
		try{
			//le o ficheiro correspondente para apanhar o access token anterior
			FileInputStream fin = new FileInputStream(API_CODE_FILE);
			ObjectInputStream ois = new ObjectInputStream(fin);
			accessToken = (Token) ois.readObject();
			ois.close();
		}catch(Exception e){
			System.out.println(Erro.File);
		}
		// ler do ficheiro o access token guardado

		Class<DropBoxApi> prov = DropBoxApi.class; 
		service = new ServiceBuilder().provider(prov).apiKey(API_KEY)
			.apiSecret(API_SECRET).scope(SCOPE).build();
		Response response = null;
		OAuthRequest request = new OAuthRequest(Verb.GET,DropAuth.REQUEST_URL.toString());
		service.signRequest(accessToken, request); //tenta entrar com o codigo anterior
		response = request.send();
		if (response.getCode() != 200){ // o codigo ja nao eh valido
			connectToService(); //liga a dropbox, e actualiza o token se necess�rio
		}
		if(name == null)
			name = "db";
		begin(name, path, csaddr);	
	}

public enum DropReq{
	METADATA("https://api.dropbox.com/1/metadata/auto/"),
	CREATE_DIR("https://api.dropbox.com/1/fileops/create_folder/?root=auto&path="),
	REMOVE_DIR("https://api.dropbox.com/1/fileops/delete/?root=auto&path="),
	UPLOAD_FILE("https://api-content.dropbox.com/1/files_put/auto/"),
	DOWNLOAD_FILE("https://api-content.dropbox.com/1/files/auto/"),
	LIST_FALSE("?list=false"),
	LIST_TRUE("?list=true"),;
	private String msg;
	private DropReq(String msg){
		this.msg = msg;
	}
	public String toString(){
		return this.msg;		
	}
};
	
public enum DropAuth{
	AUTHORIZE_URL("https://www.dropbox.com/1/oauth/authorize?oauth_token="),
	REQUEST_URL("https://api.dropbox.com/1/metadata/auto/?list=true"), 
	GetPerm("Tem de obter autorizacao para a aplicacao continuar acedendo ao link:"), 
	PressEnter("E carregar em enter quando der autorizacao");
	private String msg;
	private DropAuth(String msg){
		this.msg = msg;
	}
	public String toString(){
		return this.msg;		
	}
};

public enum Erro{
		File("Erro no ficheiro."),
		NoFile("Ficheiro nao existe."),
		Iniciar("Erro ao iniciar.");	
		String msg;
		private Erro(String msg){
			this.msg = msg;
		}
		public String toString(){
			return this.msg;		
		}
};

public static void main(String[] args) {
	getStarted(args);
	try {
		new ProxyDrop(name, path, csaddr);
	} catch (Exception e) {
		System.out.println(Erro.Iniciar);
		System.exit(0);
	}
}

/**
 * connect to service tenta ligar a dropbox.
 * � usado quando o pedido anterior tiver falhado por token expirado
 * guarda em memoria e em disco o novo token
 */
private static void  connectToService(){

			//Pedir novo codigo
			Token requestToken = service.getRequestToken();
			Verifier verifier = requestManualAuth(requestToken); 
			verifier = new Verifier(requestToken.getSecret());

			// Obter access token
			accessToken = service.getAccessToken(requestToken, verifier);
			// Guardar token  para uso futuro
			FileOutputStream fout;
			try {
				fout = new FileOutputStream(API_CODE_FILE);
				ObjectOutputStream oos = new ObjectOutputStream(fout);
				oos.writeObject(accessToken);
				oos.close();
			} catch (Exception e) {
				System.out.println(Erro.File);
			}
		}
	
/**
 * Pede ao user para manualmente autorizar o acesso da app a dropbox
 * @param requestToken � o token que faz o url de acesso especifico
 * @return o verificador que permite entender se o processo foi bem sucedido
 */
private static Verifier requestManualAuth(Token requestToken) {
		System.out.println(DropAuth.GetPerm);
		System.out.println(DropAuth.AUTHORIZE_URL + requestToken.getToken());
		System.out.println(DropAuth.PressEnter);
		System.out.print(">>");
		Scanner in = new Scanner(System.in);
		Verifier verifier = new Verifier(in.nextLine());
		in.close();
		System.out.println("Thanks");
		return verifier;
	}

public FileInfo getattr(String path) throws RemoteException,
			InfoNotFoundException {
		if(path.equalsIgnoreCase(".")) //a directoria base do servidor
			path = "";
		else
			path = path + "/";
		try{
			String URL = DropReq.METADATA.toString()+path+DropReq.LIST_FALSE;
			Response response = sendRequest(URL);
			if(response.getCode() != 200)
				return null;
			JSONParser parser = new JSONParser();
			JSONObject res;
			res = (JSONObject) parser.parse(response.getBody());
			long size=   Integer.parseInt(res.get("bytes").toString());
			String isDir = res.get("is_dir").toString();
			boolean isFile ;
			if(isDir.equalsIgnoreCase("true"))
					isFile=false;  // JSON testa se � directoria. queremos saber se � ficheiro
			else
				isFile=true;
			String modified = res.get("modified").toString(); //nao pode ser passada directamente por causa do formato
			String name = res.get("path").toString().substring(1); //path vem com uma barra antes do nome
			SimpleDateFormat df = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss ZZZZZ", Locale.US);
			Date mod = df.parse(modified);
			FileInfo fi = new FileInfo(name, size, mod, isFile);
			return fi;
		} catch (ParseException | java.text.ParseException e) {
			e.printStackTrace();
		}
		return null;	

}

public byte[] downloadFileFromServer(String path) throws RemoteException,
			InfoNotFoundException {
	if(path.equalsIgnoreCase(".")) //a directoria base do servidor nao eh um ficheiro
		return null;

	try{
		String URL = DropReq.DOWNLOAD_FILE+path;
		Response response = sendRequest(URL);
		if(response.getCode() != 200)
			return null;

		byte[] file = response.getBody().getBytes();
		return file;
	
	} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
	}

public String[] ls(String path) throws RemoteException,
			InfoNotFoundException {
		if(path.equalsIgnoreCase("."))
			path = "";
		else
			path = path + "/";
		try{
			String URL = DropReq.METADATA.toString()+path+DropReq.LIST_TRUE;
			Response response = sendRequest(URL);
			if(response.getCode() != 200)
				return null;
			JSONParser parser = new JSONParser();
			JSONObject res;
			res = (JSONObject) parser.parse(response.getBody());
			JSONArray items = (JSONArray) res.get("contents");
			@SuppressWarnings("unchecked")
			Iterator<JSONObject> it = items.iterator();
			String [] aux = new String[50]; //no m�ximo aceitamos 50 directorias/ficheiros por pasta
			int i = 0;
			while (it.hasNext()) {
				JSONObject file = it.next();
				
				aux[i] = file.get("path").toString();
				aux[i] = aux[i].substring(aux[i].lastIndexOf("/")+1);
				i++;
				}
			String [] result = new String[i];
			for(int j = 0; j < i; j++){
				result[j]  = aux [j];
			}
			
			return result;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;	

}

@Override
public boolean mkdir(String from, String dirname) throws RemoteException,
		InfoNotFoundException, MalformedURLException, NotBoundException {

	if(!isPrimary){    //se nao for primario
		if(!from.equals(primary)) //e o from nao for do primario (foi o cliente directamente a pedir)
			return false; //falha
	}
	
	else{ // eh primario		
		FileServer fs;	
		Iterator<String> it = secondaries.iterator();
		String currSrv;
		while(it.hasNext()) {
			 currSrv = it.next();
			 
			 try {
					fs = (FileServer) Naming.lookup(currSrv);
					if(!fs.mkdir(primary, dirname))
						System.out.println("SECONDARY (" + currSrv + ") refused mkdir.");
				} catch(RemoteException re) {
					System.out.println("SECONDARY (" + currSrv + ") failed with RemoteException.");
					secondaries.remove(currSrv);
				}
		}

	}			
	try{
		String URL = DropReq.CREATE_DIR+dirname;
		Response response = sendRequest(URL);
		if(response.getCode() != 200)
			return false;
		return true;
	}catch (Exception e){
		
	}
	return true;
}

@Override
public boolean mkdir(String dirname) throws RemoteException,
		InfoNotFoundException, MalformedURLException, NotBoundException {

	return mkdir("", dirname);
}

@Override
public boolean rmdir(String from, String dirname) throws RemoteException,
		InfoNotFoundException {

	if(!isPrimary){    //se nao for primario
		if(!from.equals(primary)) //e o from nao for do primario (foi o cliente directamente a pedir)
			return false; //falha
	}
	
	else{ // eh primario
		FileServer fs;
		Iterator<String> it = secondaries.iterator();
		String currSrv;
		
		while(it.hasNext()) {
			 currSrv = it.next();
			 
			 try {
					fs = (FileServer) Naming.lookup(currSrv);
					if(!fs.rmdir(primary, dirname))
						System.out.println("SECONDARY (" + currSrv + ") refused rmdir.");
				} catch(RemoteException | MalformedURLException | NotBoundException re) {
					System.out.println("SECONDARY (" + currSrv + ") failed with RemoteException.");
					secondaries.remove(currSrv);
				}
		}
		try{
			String URL = DropReq.METADATA+dirname+DropReq.LIST_TRUE;
			Response response = sendRequest(URL);
			if ( response.getCode() == 404){
				System.out.println(Erro.NoFile); // nao ha pasta com esse nome nesse path
				return false;
			}
			
			JSONParser parser = new JSONParser();
			JSONObject res;
			res = (JSONObject) parser.parse(response.getBody());
			String isDir = res.get("is_dir").toString();
			if(isDir.equalsIgnoreCase("false"))
					return false;  // para este comando apenas apagar pastas
			else
			{
				JSONArray items = (JSONArray) res.get("contents");
				if(!items.isEmpty())
					return false; //para so apagar uma pasta se estiver vazia
				URL = DropReq.REMOVE_DIR+dirname;
				response = sendRequest(URL);
				if(response.getCode() != 200)
					return false;
				return true;
			}
		}catch (Exception e){
		}

	}
	return false;	
}

@Override
public boolean rmdir(String dirname) throws RemoteException,
		InfoNotFoundException {
	return rmdir("", dirname);
}

@Override
public boolean rm(String from, String dirname) throws RemoteException,
		InfoNotFoundException {

	
	if(!isPrimary){    //se nao for primario
		if(!from.equals(primary)) //e o from nao for do primario (foi o cliente directamente a pedir)
			return false; //falha
	}
	
	else{ // eh primario
		
		FileServer fs;
		Iterator<String> it = secondaries.iterator();
		String currSrv;
		
		while(it.hasNext()) {   // se tiver secundarios tem de propagar a operacao para eles
			 currSrv = it.next(); 
			 
			 try {
					fs = (FileServer) Naming.lookup(currSrv);
					if(!fs.rm(primary, dirname))
						System.out.println("SECONDARY (" + currSrv + ") refused rm.");
				} catch(RemoteException | MalformedURLException | NotBoundException re) {
					System.out.println("SECONDARY (" + currSrv + ") failed with RemoteException.");
					secondaries.remove(currSrv);
				}
		}
	}
	//ao chegar aqui, temos a garantia que eh primario ou que eh secundario ordenado pelo primario
	//portanto podemos fazer a operacao
	try{
		String URL = DropReq.METADATA+dirname; //primeiro tem de pedir os atributos do path dado
		Response response = sendRequest(URL);
		if ( response.getCode() == 404){  //representa a existencia do ficheiro
			System.out.println(Erro.NoFile); 
			return false;
		}
		
		JSONParser parser = new JSONParser();
		JSONObject res;
		res = (JSONObject) parser.parse(response.getBody()); //reccolhe os metadados
		String isDir = res.get("is_dir").toString(); //is_dir pode ser true ou false
		if(isDir.equalsIgnoreCase("true"))
				return false;  // JSON testa se � directoria. queremos saber se � ficheiro
								//queremos que este metodo apenas remova se for ficheiro
		else
		{
			URL = DropReq.REMOVE_DIR+dirname;  //remove o ficheiro
			response = sendRequest(URL);
			if(response.getCode() != 200)  //sucesso
				return false;
			return true;
		}
	}catch (Exception e){
	}
	
	return false;
}

@Override
public boolean rm(String path) throws RemoteException,
		InfoNotFoundException {
	return rm("", path);
}

@Override
public boolean uploadFileToServer(String from, byte[] file, String filename,
		String toPath) throws RemoteException, InfoNotFoundException {

	if(!isPrimary){    //se nao for primario
		if(!from.equals(primary)) //e o from nao for do primario (foi o cliente directamente a pedir)
			return false; //falha
	}
	
	else{ // eh primario
		FileServer fs;
		Iterator<String> it = secondaries.iterator();
		String currSrv;
		
		while(it.hasNext()) {   // se tiver secundarios tem de propagar a operacao para eles
			 currSrv = it.next(); 
			 
			 try {
					fs = (FileServer) Naming.lookup(currSrv);
					if(!fs.uploadFileToServer(primary, file, filename, toPath))
						System.out.println("SECONDARY (" + currSrv + ") refused uploadFileToServer.");
				} catch(RemoteException | MalformedURLException | NotBoundException re) {
					System.out.println("SECONDARY (" + currSrv + ") failed with RemoteException.");
					secondaries.remove(currSrv);
				}
		}
		String path;
		if(toPath.equalsIgnoreCase(".")) //a directoria base da dropbox
			path = filename;
		else
			path = toPath + "/" + filename;
		try{
			String URL = DropReq.UPLOAD_FILE+path;
			Response response = sendPutRequest(URL,file);
			if(response.getCode() == 200)
				return true;
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	return false;

}

public boolean uploadFileToServer(byte[] file, String filename, String toPath)
		throws RemoteException, InfoNotFoundException {
	return uploadFileToServer("",file, filename, toPath);
}

private Response sendRequest(String URL) {
	OAuthRequest request = new OAuthRequest(Verb.GET,URL);
	service.signRequest(accessToken, request); //tenta entrar com o codigo anterior
	Response response = request.send();
	if (response.getCode() == 401){ // access token expirado
		connectToService();
		service.signRequest(accessToken, request); //tenta entrar com o novo codigo
		request = new OAuthRequest(Verb.GET,URL);
		response = request.send();
	}
	return response;
	
}

private Response sendPutRequest(String URL,byte[] file) {
	OAuthRequest request = new OAuthRequest(Verb.PUT,URL);
	service.signRequest(accessToken, request); //tenta entrar com o codigo anterior
	request.addHeader("Content-Type","");
	request.addPayload(file);
	Response response = request.send();
	if (response.getCode() == 401){ // access token expirado
		connectToService();
		service.signRequest(accessToken, request); //tenta entrar com o novo codigo
		request = new OAuthRequest(Verb.PUT,URL);
		request.addHeader("Content-Type","");
		request.addPayload(file);
		response = request.send();
	}
	return response;
	
}

}
