

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.Iterator;


public class FileServerImpl extends Server {

	private static final long serialVersionUID = 1L;

	protected FileServerImpl(String name, String path, String csaddr) throws IOException, NotBoundException {
		
		super();	
		if(name == null)
			name = "sd";
		begin(name, path, csaddr);
	}
	
	public static void main( String args[]) {

		// Arguments: FileServerImpl serverName[arg0] (e.g. FileServerImpl SD)
		
		try {
			getStarted(args);
			new FileServerImpl(name,path,csaddr);
		} catch (Exception e) {
			System.out.println("erro ao iniciar.");
			System.exit(0);
		} 
	}

	public String[] ls(String path) throws RemoteException, InfoNotFoundException {

		if(path == null)
			path = ".";
		File f = new File(basePath, path);
		
		if(f.exists())
			return f.list(fileutils.Filters.getHiddenFilter());
		else
			throw new InfoNotFoundException( "Directory not found :" + path);
	}

	public boolean mkdir(String from, String dirname) throws RemoteException,
			InfoNotFoundException, MalformedURLException, NotBoundException {
		
		if(!isPrimary){    //se nao for primario
			if(!from.equals(primary) && !from.equals(self)) //e o from nao for do primario (foi o cliente directamente a pedir)
				return false; //falha
		}
		else{
			
			FileServer fs;
			Iterator<String> it = secondaries.iterator();
			String currSrv;
			
			while(it.hasNext()) {
				 currSrv = it.next();
				 
				 try {
						fs = (FileServer) Naming.lookup(currSrv);
						if(!fs.mkdir(primary, dirname))
							System.out.println("SECONDARY (" + currSrv + ") refused mkdir operation.");
					} catch(RemoteException re) {
						System.out.println("SECONDARY (" + currSrv + ") failed with RemoteException.");
						secondaries.remove(currSrv);
					}
			}
		}
		return new File(basePath, dirname).mkdir();
	}
	
	@Override
	public boolean mkdir(String dirname) throws RemoteException,
			InfoNotFoundException, MalformedURLException, NotBoundException {

		return mkdir("", dirname);
	}

	@Override
	public boolean rmdir(String from, String dirname) throws RemoteException,
			InfoNotFoundException {
		if(!isPrimary){    //se nao for primario
			if(!from.equals(primary) && !from.equals(self)) //e o from nao for do primario (foi o cliente directamente a pedir)
				return false; //falha
		}
		else{
			
			FileServer fs;
			Iterator<String> it = secondaries.iterator();
			String currSrv;
			
			while(it.hasNext()) {
				 currSrv = it.next();
				 
				 try {
						fs = (FileServer) Naming.lookup(currSrv);
						if(!fs.rmdir(primary, dirname))
							System.out.println("SECONDARY (" + currSrv + ") refused rmdir operation.");
					} catch(RemoteException | MalformedURLException | NotBoundException re) {
						System.out.println("SECONDARY (" + currSrv + ") failed with RemoteException.");
						secondaries.remove(currSrv);
					}
			}
		}
		File f = new File(basePath, dirname);
		if(!f.isFile())
			return f.delete();
		return false;
	}
	
	@Override
	public boolean rmdir(String dirname) throws RemoteException,
			InfoNotFoundException {
		return rmdir("", dirname);
	}

	@Override
	public boolean rm(String from, String path) throws RemoteException,
			InfoNotFoundException {
		if(!isPrimary){    //se nao for primario
			if(!from.equals(primary) && !from.equals(self)) //e o from nao for do primario (foi o cliente directamente a pedir)
				return false; //falha
		}
		else{
			
			FileServer fs;
			Iterator<String> it = secondaries.iterator();
			String currSrv;
			
			while(it.hasNext()) {
				 currSrv = it.next();
				 
				 try {
						fs = (FileServer) Naming.lookup(currSrv);
						if(!fs.rm(primary, path))
							System.out.println("SECONDARY (" + currSrv + ") refused rm operation.");
					} catch(RemoteException | MalformedURLException | NotBoundException re) {
						System.out.println("SECONDARY (" + currSrv + ") failed with RemoteException.");
						secondaries.remove(currSrv);
					}
			}
		}
		File f = new File(basePath, path);
		if(f.isFile()){
			return f.delete();
		}
		return false;
	}
	
	@Override
	public boolean rm(String path) throws RemoteException,
			InfoNotFoundException {
		return rm("", path);
	}

	@Override
	public FileInfo getattr(String path) throws RemoteException,
			InfoNotFoundException {
		File f = new File(basePath,path);
		if( f.exists()) 
			return new FileInfo(f.getName(), f.length(), new Date(f.lastModified()), f.isFile());
		else
			throw new InfoNotFoundException( "File not found");
	}
	
	@Override
	public boolean uploadFileToServer(String from, byte[] file, String filename, String toPath) throws RemoteException, InfoNotFoundException{ //
		if(!isPrimary){    //se nao for primario
			if(!from.equals(primary) && !from.equals(self)) //e o from nao for do primario (foi o cliente directamente a pedir)
				return false; //falha
		}
		else{
			
			FileServer fs;
			Iterator<String> it = secondaries.iterator();
			String currSrv;
			
			while(it.hasNext()) {
				 currSrv = it.next();
				 
				 try {
						fs = (FileServer) Naming.lookup(currSrv);
						if(!fs.uploadFileToServer(primary,file,filename, toPath))
							System.out.println("SECONDARY (" + currSrv + ") refused upload of file.");
					} catch(RemoteException | MalformedURLException | NotBoundException re) {
						System.out.println("SECONDARY (" + currSrv + ") failed with RemoteException.");
						secondaries.remove(currSrv);
					}
			}
		}
		try {
			Path dir = Paths.get(basePath.toString()+"/"+toPath);
			Path path = dir.resolve(filename);
			Files.createDirectories(path.getParent());
			if(Files.createFile(path)==null)
				return false;
			Files.write(path, file);
			return true;
		} catch( Exception e) {
			return false;
		}
		
	}
	
	@Override
	public boolean uploadFileToServer(byte[] file, String filename, String toPath) throws RemoteException, InfoNotFoundException{ //
		return uploadFileToServer("", file, filename, toPath);
	}

	@Override
	public byte[] downloadFileFromServer(String fromPath) throws RemoteException, InfoNotFoundException{
		try {
			File f = new File(basePath,fromPath);
			if(f.exists()&&f.isFile()){
				RandomAccessFile raf = new RandomAccessFile(f, "r");
				byte[] file = new byte [(int) f.length()];
				raf.readFully(file);
				raf.close();
				return file;
			}
			return null;
		} catch( Exception e) {
			return null;
		}
	}
}