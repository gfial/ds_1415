

import java.sql.Timestamp;

public class ServerInfo implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public String url, name;
	public Timestamp lastSeen;
	
	public ServerInfo( String name, String url, Timestamp lastSeen) {
		this.name = name;
		this.url = url;
		this.lastSeen = lastSeen;
	}
	
	public String getUrl() {
		
		return url;
	}
	
	public String getName() {
		return name;
	}
	
	public Timestamp getTimestamp() {
		return lastSeen;
	}
	
	public String toString() {

		return url; 
	}
}
