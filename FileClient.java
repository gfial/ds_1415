
import fileutils.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.HashMap;

/**
 * Classe base do cliente
 * @author
 */
public class FileClient
{
	public static final String SYNC_DIR_PATH = "./.sync";
	public static final String SYNC_LAST_MOD_SERVER = "./.sync/server.ser";
	public static final String SYNC_LAST_MOD_CLIENT = "./.sync/client.ser";
	public static final String CONFLICT_EXT = ".local";
	private static final String PATH = "clientFolder";
	private HashMap<String, Date> lastModifiedServer, lastModifiedClient;
	private File basePath;
	String contactServerURL;
	ContactServer cs;

	public FileClient(ContactServer server, String path) throws InfoNotFoundException {
		
		super();
		
		// Create directory for the client to operate on
		if(path == null) {
			basePath = new File(".");
			new File(basePath, PATH).mkdir();
			basePath = new File(PATH);	
		}
		
		else
			basePath = new File(path);
		
		if(!basePath.exists())
			throw new InfoNotFoundException("The specified path \""+basePath.getAbsolutePath()+"\" does not exist");
		
		lastModifiedServer = new HashMap<String, Date>();
		lastModifiedClient = new HashMap<String, Date>();
		cs = server;
		
		if(loadMeta())
			System.out.println("Meta-data found and loaded.");
		else
			System.out.println("No meta-data loaded.");
	}


	public static void main( String[] args) {
				
		System.out.println("FileClient v1.2");
		
		try {
			if(args.length==0) //iniciado sem indica��o de pasta, 
				new FileClient(seekContactServer(null),null).doit(); //a pasta a ser usada ser�  a raiz onde esta a ser executada
			else if(args.length==1)
				new FileClient(seekContactServer(null),args[0]).doit();
			else if(args.length == 2)
				new FileClient(seekContactServer(args[1]),args[0]).doit();
			else{
				throw new InfoNotFoundException(" use java FileServerImpl server_name \n\t or java FileServerImpl server_name working_path");
			}
		} catch (Exception e) {
			System.err.println("\nError: " + e.getMessage()); //Aqui sao mostrados os inputs invalidos
			e.printStackTrace(System.err);
		}
	}


	/**
	 * Devolve um array com os servidores a correr caso o name== null ou o URL dos
	 * servidores com nome name.
	 */
	protected String[] servers( String name) throws Exception {
		
		return cs.listServers(name);
	}
	
	/**
	 * Devolve um array com os ficheiros/directoria na directoria dir no servidor server
	 * (ou no sistema de ficheiros do cliente caso server == null).
	 * Se isURL for verdadeiro, server representa um URL para o servidor (e.g. //127.0.0.1/myServer).
	 * Caso contrario e o nome do servidor. Nesse caso deve listar os ficheiros dum servidor com esse nome.
	 * Devolve null em caso de erro.
	 * NOTA: nao deve lancar excepcao. 
	 */
	protected String[] dir( String server, boolean isURL, String dir) {
		//System.err.println( "exec: ls " + dir + " no servidor " + server + " - e url : " + isURL);
		FileServer fs;
		
		try {

				if(server == null){
					File f = new File(basePath, dir);
					
					if(f.exists())
						return f.list(Filters.getHiddenFilter());
					else
						throw new InfoNotFoundException( "Directory not found :" + dir);
				}
				else if(isURL) {
					fs = (FileServer) Naming.lookup(server);
					return fs.ls(dir);
				}
				else{  //foi passado nome de servidor
					String[] servs = cs.listServers(server);
					int i = 0;
					while(i<servs.length){
						try{
							fs = (FileServer) Naming.lookup(servs[i]);
							return fs.ls(dir);
						
						}catch(Exception e){
							i++;
						}
					}
					return null; //esgotou a lista de servidores com esse nome sem que nenhum responda
						
				}
			
		} catch (Exception e) {
			return null;
		}
		
		
	}
	
	/**
	 * Cria a directoria dir no servidor server@user
	 * (ou no sistema de ficheiros do cliente caso server == null).
	 * Se isURL for verdadeiro, server representa um URL para o servidor (e.g. //127.0.0.1/myServer).
	 * Caso contrario e o nome do servidor.
	 * Devolve false em caso de erro.
	 * NOTA: nao deve lancar excepcao. 
	 */
	protected boolean mkdir( String server, boolean isURL, String dir) {
		try {
			if (server != null){
				FileServer fs;
				if(isURL) {
					fs = (FileServer) Naming.lookup(server);
					return fs.mkdir(dir);
				}
				
				fs = (FileServer) Naming.lookup(cs.getPrimaryServer(server));
				return fs.mkdir(dir);
			}
			else{ //a directoria eh local
				return new File(basePath,dir).mkdir();
			}
		}catch (Exception e) {
			return false;
		}	
		//System.err.println( "exec: mkdir " + dir + " no servidor " + server +" - e url : " + isURL);
		
	}

	/**
	 * Remove a directoria dir no servidor server@user
	 * (ou no sistema de ficheiros do cliente caso server == null).
	 * Se isURL for verdadeiro, server representa um URL para o servidor (e.g. //127.0.0.1/myServer).
	 * Caso contrario e o nome do servidor.
	 * Devolve false em caso de erro.
	 * NOTA: nao deve lancar excepcao. 
	 */
	protected boolean rmdir( String server, boolean isURL, String dir) {
		//System.err.println( "exec: mkdir " + dir + " no servidor " + server + " - e url : " + isURL);
		try {
			if (server != null){
				FileServer fs;
				if(isURL)
					fs = (FileServer) Naming.lookup(server);
				else 
					fs = (FileServer) Naming.lookup(cs.getPrimaryServer(server));
				return fs.rmdir(dir);
			}
			else{
				File f =  new File(basePath,dir);
				if(!f.isFile())
					return f.delete();
			}
			return false;
		}catch (Exception e) {
			return false;
		}	
	}

	/**
	 * Remove o fichseiro path no servidor server@user.
	 * (ou no sistema de ficheiros do cliente caso server == null).
	 * Se isURL for verdadeiro, server representa um URL para o servidor (e.g. //127.0.0.1/myServer).
	 * Caso contrario e o nome do servidor.
	 * Devolve false em caso de erro.
	 * NOTA: nao deve lancar excepcao. 
	 */
	protected boolean rm( String server, boolean isURL, String path) {
		//System.err.println( "exec: rm " + path + " no servidor " + server +" - e url : " + isURL);
		try {
			if (server != null){
				FileServer fs;
				if(isURL)
					fs = (FileServer) Naming.lookup(server);
				else 
					fs = (FileServer) Naming.lookup(cs.getPrimaryServer(server));
				return fs.rm(path);	
			}
			else{
				
				File f = new File (basePath ,path);
				if (f.isFile())
					return f.delete();
			}
			return false;
		}catch (Exception e) {
			return false;
		}	
	}

	/**
	 * Devolve informacao sobre o ficheiro/directoria path no servidor server@user.
	 * (ou no sistema de ficheiros do cliente caso server == null).
	 * Se isURL for verdadeiro, server representa um URL para o servidor (e.g. //127.0.0.1/myServer).
	 * Caso contrario e o nome do servidor.
	 * Devolve false em caso de erro.
	 * NOTA: nao deve lancar excepcao. 
	 */
	protected FileInfo getAttr( String server, boolean isURL, String path) {
		//System.out.println( "exec: getattr " + path +  " no servidor " + server + " - e url : " + isURL);
		try {
				if (server != null)
					return getServerStub(server, isURL, false).getattr(path);
				
				else {
					File f = new File(basePath, path);
					if (f.exists())
						return new FileInfo(f.getName(),f.length(), new Date(f.lastModified()), f.isFile());
				}
			return null;
		}catch (Exception e) {
			return null;
		}	
	}

	/**
	 * Copia ficheiro de fromPath no servidor fromServer@fromUser para o ficheiro 
	 * toPath no servidor toServer@toUser.
	 * (caso fromServer/toServer == local, corresponde ao sistema de ficheiros do cliente).
	 * Devolve false em caso de erro.
	 * NOTA: nao deve lancar excepcao. 
	 */
	protected boolean cp( String fromServer, boolean fromIsURL, String fromPath,
							String toServer, boolean toIsURL, String toPath) {
	//	System.err.println( "exec: cp " + fromPath + " no servidor " + fromServer + " - e url : " + fromIsURL + " para " +
		//		toPath + " no servidor " + toServer +" - e url : " + toIsURL);
		try{
			byte[] file = null;
			if(fromServer==null){ // a origem e uma diretoria local
				File f = new File(basePath,fromPath);
				if(f.exists()&&f.isFile()){
					RandomAccessFile raf = new RandomAccessFile(f, "r");
					file = new byte [(int) f.length()];
					raf.readFully(file);
					raf.close();
				}
			}
			
			else
				file = getServerStub(fromServer, fromIsURL, false).downloadFileFromServer(fromPath);
			
			if (file == null)
				return false;
			
			String[] fn = fromPath.split("/");
			String filename = fn [fn.length-1];
			if(toServer==null){ //destino eh local
				Path dir = Paths.get(basePath.toString()+"/"+toPath);
				Path path = dir.resolve(filename);
				Files.createDirectories(path.getParent());
				if(Files.createFile(path)==null)
					return false;
				Files.write(path, file);
				return true;
			}
			
			else //destino � servidor
				return getServerStub(toServer, toIsURL, true).uploadFileToServer(file, filename, toPath);
		
		}catch (Exception e) {
			return false;
		}
		
		
	}
	
	/**
	 * Syncs the client folder with a server (primary).
	 * @param cpath - The path of the directory in the client to be synced.
	 * @param server - The name or URL of the server to sync with.
	 * @param isUrl - Indicate if the <b>server</b> is a name or a URL format.
	 * @param path - The path to the directory in the server to sync with.
	 * @return <b>true</b> if the operation was sucessful.
	 */
	protected boolean sync(String cpath, String server, boolean isUrl, String path) {
		
		FileServer fs;
		File f = new File(basePath, cpath);
		
		try {
			
			// If client DIR is empty, not even .sync
			if(f.list().length == 0) {
				fs = getServerStub(server, isUrl, false);
				if(replicateFromServer(cpath, path, fs)) {
					saveMeta();
					return true;
				}
				System.out.println("sync: Failed to replicate from server.");
				return false;
			}
			
			// Client has contents
			else {
				fs = getServerStub(server, isUrl, true);
				
				// If server DIR is empty
				try {
					if(fs.ls(path).length == 0) {
						if(replicateToServer(cpath, path, fs)) {
							saveMeta();
							return true;
						}
						System.out.println("sync: Failed to replicate to server.");
						return false;
					}
				} catch (RemoteException e) {
					System.out.println("sync: " + server + " (primary) became unreachable.");
					return false;
				} catch (InfoNotFoundException e) {
					System.out.println("sync: given " + path + " leads nowhere.");
					return false;
				}
				
				// Sync
				if(syncdir(cpath, path, fs) && syncmissing(cpath, path, fs) && saveMeta())
					return true;
				
				}
			
		} catch(InfoNotFoundException e) {
			System.out.println("sync: "+cpath+" file not found.");
		} catch(RemoteException e) {
			System.out.println("sync: Failed to contact server.");
		}
		
		return false;
	}
	
	/**
	 * Synchronizes the client directory <b>cpath</b> with the server directory <b>spath</b>.
	 * @param cbasepath - The path of the client directory.
	 * @param sbasepath - The path of the server directory.
	 * @param fs - The stub to the server.
	 * @return <b>true</b> if the operation was sucessful, <b>false</b> otherwise.
	 * @throws InfoNotFoundException 
	 * @throws RemoteException 
	 */
	private boolean syncdir(String cbasepath, String sbasepath, FileServer fs) throws RemoteException {
		
		//String[] serverfiles = fs.ls(spath);
		String[] clientfiles = dir(null, false, cbasepath);
		
		String sfilepath, cfilepath, filename;
		Date lastModClientSinceSync, lastModClient, lastModServerSinceSync;
		FileInfo serverFile;
		
		// ALL files obviously existent in client
		for(int i = 0; i < clientfiles.length; i++) {
			
				filename = clientfiles[i];
				
				// .local files shall not sync
				if(filename.endsWith(CONFLICT_EXT))
					continue;
				
				sfilepath = sbasepath + "/" + filename;
				cfilepath = cbasepath + "/" + filename;
				lastModClientSinceSync = lastModifiedClient.get(cfilepath);
				lastModServerSinceSync = lastModifiedServer.get(sfilepath);
				lastModClient = getAttr(null, false, cfilepath).modified;
				
				try {
				try {
				serverFile = fs.getattr(sfilepath);
				} catch(InfoNotFoundException e) {
					serverFile = null;
				}
				
				// If file to be synced is a normal file
				if( getAttr(null, false, cfilepath).isFile) {
					
					// Doesnt exist in server
					if(serverFile == null) {
						
						// Only client has NEW file
						if(lastModClientSinceSync == null) {
							
							// Create in server
							if(!cp(cfilepath, sbasepath, null, fs)) {
								System.out.println("sync: Failed to send " + cfilepath + " to " + sbasepath);
								return false;
							}
							
							lastModifiedClient.put(cfilepath, lastModClient);
							lastModifiedServer.put(sfilepath, fs.getattr(sfilepath).modified);
						}
						
						// If file was not updated since last sync
						else if(lastModClient.equals(lastModClientSinceSync)) {
							if(!rm(null, false, cfilepath)) {
								System.out.println("sync: Failed to remove " + cfilepath);
								return false;
							}
							lastModifiedClient.remove(cfilepath);
							lastModifiedServer.remove(sfilepath);
						}
					
						// Modified since last sync, copy to server again
						else {
							if(!cp(cfilepath, sbasepath, null, fs)) {
								System.out.println("sync: Failed to send " + cfilepath + " to " + sbasepath);
								return false;
							}
							
							lastModifiedClient.put(cfilepath, lastModClient);
							lastModifiedServer.put(sfilepath, fs.getattr(sfilepath).modified);
						}
					}
					
					// File exists in server
					else {
						
						// Client and Server BOTH have a NEW file with same name
						if(lastModServerSinceSync == null) {
							
							rm(null, false, cfilepath + CONFLICT_EXT); //delete a previous .local
							rename(cfilepath, CONFLICT_EXT, true);
							rm(null, false, cfilepath);
							cp(sfilepath, cbasepath, fs, null);
							
							lastModifiedClient.put(cfilepath, lastModClient);
							lastModifiedServer.put(sfilepath, fs.getattr(sfilepath).modified);
						}
						
						// If client updated
						else if(lastModClientSinceSync.compareTo(lastModClient) < 0) {
							
							// If server updated aswell
							if(lastModServerSinceSync.compareTo(serverFile.modified) < 0) {
								rm(null, false, cfilepath + CONFLICT_EXT);
								rename(cfilepath, CONFLICT_EXT, true);
								rm(null, false, cfilepath);
								cp(sfilepath, cbasepath, fs, null);
							}
							
							// If server was NOT updated since
							else {
								try {
									fs.rm(sfilepath);
								} catch(Exception e) {
									System.out.println("sync: Failed to remove "+sfilepath+" (outdated version) from server.");
									return false;
								}
								if(!cp(cfilepath, sbasepath, null, fs)) {
									System.out.println("sync: Failed to send " + cfilepath + " to " + sbasepath);
									return false;
								}
							}
							
							lastModifiedClient.put(cfilepath, lastModClient);
							lastModifiedServer.put(sfilepath, fs.getattr(sfilepath).modified);
						}
					}
				}
				
				// If file to be synced is a directory
				else {
					
					// Dir doesnt exist in server
					if(serverFile == null) {
						
						// If dir is NEW in client OR was modified by client
						if(lastModServerSinceSync == null || !(lastModClient.equals(lastModClientSinceSync))) {
							
							try {
								fs.mkdir(sfilepath);
							} catch (Exception e) {
								System.out.println("sync: Failed to create server directory "+sfilepath);
								return false;
							}
							
							lastModifiedClient.put(cfilepath, lastModClient);
							lastModifiedServer.put(sfilepath, fs.getattr(sfilepath).modified);
							syncdir(cfilepath, sfilepath, fs);
						}
						
						// If dir was not updated since last sync
						else {
							
							// If dir in client is empty (remove dir)
							if(dir(null, false, cfilepath).length == 0) {
								if(!rmdir(null, false, cfilepath)) {
									System.out.println("sync: Failed to remove directory " + cfilepath);
									return false;
								}
								lastModifiedClient.remove(cfilepath);
								lastModifiedServer.remove(sfilepath);
							}
							
							// Recursion to clean dir from inside
							else {
								syncdir(cfilepath, sfilepath, fs);
								
								// If dir ended up empty (remove dir)
								if(dir(null, false, cfilepath).length == 0) {
									if(!rmdir(null, false, cfilepath)) {
										System.out.println("sync: Failed to remove directory " + cfilepath);
										return false;
									}
									lastModifiedClient.remove(cfilepath);
									lastModifiedServer.remove(sfilepath);
								}
							}
						}
					}
					// Dir exists in server
					else {
						lastModifiedClient.put(cfilepath, lastModClient);
						lastModifiedServer.put(sfilepath, fs.getattr(sfilepath).modified);
						syncdir(cfilepath, sfilepath, fs);
					}
				}
			} catch(InfoNotFoundException e) {
				System.out.println("sync: "+sfilepath+" file not found in server.");
			}
		}
		return true;
	}
	
	/**
	 * Intended to be used after syncdir to process NEW files added to the server<br/>
	 * by other client and not added to the current syncing client.
	 * @return If the operation was sucessful.
	 * @throws InfoNotFoundException 
	 * @throws RemoteException 
	 */
	private boolean syncmissing(String cbasepath, String sbasepath, FileServer fs) throws RemoteException, InfoNotFoundException {
	
		//Add missing files that were added new to server but not to client
		String[] serverfiles = fs.ls(sbasepath);
		
		String sfilepath, cfilepath, filename;
		Date lastModClientSinceSync, lastModServerSinceSync;
		FileInfo clientFile, serverFile;
		
		// iterates ALL files that obviously exist in server
		for(int i = 0; i < serverfiles.length; i++) {
			
			filename = serverfiles[i];
			sfilepath = sbasepath + "/" + filename;
			cfilepath = cbasepath + "/" + filename;
			clientFile = getAttr(null, false, cfilepath);
			
			// If a file does not exist in client
			if(clientFile == null) {
				lastModClientSinceSync = lastModifiedClient.get(cfilepath);
				lastModServerSinceSync = lastModifiedServer.get(sfilepath);
				serverFile = fs.getattr(sfilepath);
				
				// NEW file is NOT a directory
				if(serverFile.isFile) {
					
					// Never existed before in client
					if(lastModClientSinceSync == null) {
						
						if(!cp(sfilepath, cbasepath, fs, null)) {
							System.out.println("sync: Failed to copy from server ("+sfilepath+") to client ("+cbasepath+")");
							return false;
						}
						lastModifiedClient.put(cfilepath, getAttr(null, false, cfilepath).modified);
						lastModifiedServer.put(sfilepath, serverFile.modified);
					}
					
					// Existed before in client, client removed it after last sync
					else {
						// File was not modified on server after last sync, delete from server
						if(lastModServerSinceSync.equals(serverFile.modified)) {
							
							try {
								fs.rm(sfilepath);
							} catch (MalformedURLException | NotBoundException e) {
								System.out.println("sync: Failed to remove "+sfilepath+" from server.");
								return false;
							}
							lastModifiedClient.remove(cfilepath);
							lastModifiedServer.remove(sfilepath);
						}
						
						// File modified on server since last sync, copy to client
						else {
							
							if(!cp(sfilepath, cbasepath, fs, null)) {
								System.out.println("sync: Failed to copy from server ("+sfilepath+") to client ("+cbasepath+")");
								return false;
							}
							lastModifiedClient.put(cfilepath, getAttr(null, false, cfilepath).modified);
							lastModifiedServer.put(sfilepath, serverFile.modified);
						}
					}
				}
				
				// NEW file is directory
				else {
					// If dir is NEW in server
					//OR was deleted by client and server modified it
					// || !(serverFile.modified.equals(lastModServerSinceSync))
					if(lastModClientSinceSync == null ) {
					
						if(!mkdir(null, false, cfilepath)) {
							System.out.println("sync: Failed to create local directory "+cfilepath);
							return false;
						}
						
						lastModifiedClient.put(cfilepath, getAttr(null, false, cfilepath).modified);
						lastModifiedServer.put(sfilepath, serverFile.modified);
						syncmissing(cfilepath, sfilepath, fs);
					}
					
					// If dir existed in client at last sync (client removed it)
					else {
						
						// If dir in server is empty (remove dir)
						if(fs.ls(sfilepath).length == 0) {
							try {
								if(fs.rmdir(sfilepath)) {
									System.out.println("sync: Failed to remove server directory " + sfilepath);
									return false;
								}
							} catch (MalformedURLException | NotBoundException e) {
								System.out.println("sync: Failed to remove server directory " + sfilepath);
								return false;
							}
							lastModifiedClient.remove(cfilepath);
							lastModifiedServer.remove(sfilepath);
						}
						
						// IF not empty, recursion to clean dir from inside, or avoid deletion in case of new/modified files inside
						else {
							syncmissing(cfilepath, sfilepath, fs);
							
							// If dir ended up empty, no modified/new files (remove dir)
							if(fs.ls(sfilepath).length == 0) {
								try {
									if(!fs.rmdir(sfilepath)) {
										System.out.println("sync: Failed to remove server directory " + sfilepath);
										return false;
									}
								} catch (MalformedURLException | NotBoundException e) {
									System.out.println("sync: Failed to remove server directory " + sfilepath);
									return false;
								}
								lastModifiedClient.remove(cfilepath);
								lastModifiedServer.remove(sfilepath);
							}
						}
					}
				}
			}
			// If a folder exists in client (may not contain what server contains in the same folder)
			else if(!clientFile.isFile){
				syncmissing(cfilepath, sfilepath, fs);
			}
		}
		return true;
	}
	
	/**
	 * Replicates the contents of the client at folder <b>cpath</b> (<i>null</i> provides the default base dir)<br/>
	 * to the server at folder <b>spath</b>.
	 * @param cbasepath - The path to the client folder to provide the data, leave <i>null</i> for the default.
	 * @param sbasepath - The path to the server directory intended to receive the data.
	 * @param fs - The server stub.
	 * @return If the operation was sucessful or not.
	 */
	private boolean replicateToServer(String cbasepath, String sbasepath, FileServer fs) {
		
		if(cbasepath == null)
			cbasepath = ".";
			
		String[] clientfiles = dir(null, false, cbasepath);
		
		for(int i = 0; i < clientfiles.length; i++) {
			
			String sfilepath = sbasepath + "/" + clientfiles[i];
			String cfilepath = cbasepath + "/" + clientfiles[i];
			
			FileInfo file = getAttr(null, false, cfilepath);
			
			if(file == null) {
				System.out.println("Replication: "+cfilepath+" not returning information.");
				return false;
			}
			
			// If file to be synced is a normal file
			if(file.isFile) {
				if(!cp(cfilepath, sbasepath, null, fs)) {
					System.out.println("Replication: Error copying " + cfilepath + " to server.");
					return false;
				}
			}
			
			// If file to be synced is a directory
			else {
				
				try {
					fs.mkdir(sfilepath);
				} catch (Exception e) {
					System.out.println("Replication: Error making remote directory: " + sfilepath);
					return false;
				}
				
				if(!replicateToServer(cfilepath, sfilepath, fs))
					return false;
			}
			
			try {
				lastModifiedServer.put(sfilepath, fs.getattr(sfilepath).modified);
			} catch (Exception e) {
				System.out.println("Replication: Failed to get attributes from server file: "+sfilepath);
				return false;
			}
			lastModifiedClient.put(cfilepath, getAttr(null, false, cfilepath).modified);
				
		}
		
		return true;
	}
	
	/**
	 * Replicates the contents of the server at folder <b>spath</b><br/>
	 * to the client at folder <b>cpath</b>.
	 * @param cbasepath - The path to the client folder to receive the data, leave <i>null</i> for the default.
	 * @param sbasepath - The path to the server directory where to replicate the data from.
	 * @param fs - The server stub.
	 * @return If the operation was sucessful or not.
	 */
	private boolean replicateFromServer(String cbasepath, String sbasepath, FileServer fs) {
		
		if(cbasepath == null)
			cbasepath = ".";
			
		String[] serverfiles;
		
		try {
			serverfiles = fs.ls(sbasepath);
			
			for(int i = 0; i < serverfiles.length; i++) {
				
				String sfilepath = sbasepath + "/" + serverfiles[i];
				String cfilepath = cbasepath + "/" + serverfiles[i];
				FileInfo remoteFileInfo = fs.getattr(sfilepath);
				
				// If file to be synced is a normal file
				if(remoteFileInfo.isFile) {
					if(!cp(sfilepath, cbasepath, fs, null)) {
						System.out.println("Replication: Error copying " + sfilepath + " from server.");
					}
				}
				
				// If file to be synced is a directory
				else {
					
					// Make the directory in the client folder
					if(!mkdir(null, false, cfilepath)) {
						System.out.println("Replication: Error making local directory: " + cfilepath);
						return false;
					}
					
					// Recursively copy files into the new directory
					if(!replicateFromServer(cfilepath, sfilepath, fs))
						return false;
				}
				
				lastModifiedServer.put(sfilepath, remoteFileInfo.modified);
				try {
				lastModifiedClient.put(cfilepath, getAttr(null, false, cfilepath).modified);
				} catch (Exception e) {
					System.out.println("Replication: Failed to get attributes from file: "+cfilepath);
				}
			}
		} catch (Exception e) {
			System.out.println("Replication: Error replicating files.");
			e.printStackTrace(System.err);
			return false;
		}
		
		return true;
	}
	
	/**
	 * Copies a file from one server to another server, client to server ou server to client.
	 * @param fromPath - Path poiting to the file to be copied.
	 * @param toPath - Path poiting to the location where the file will be copied to.
	 * @param fromServer - Server from which the file is going to be copied, <i>null</i> if client.
	 * @param toServer - Server from which the file is going to be copied to, <i>null</i> if client.
	 * @return <b>true</b> if the operation was sucessful, <b>false</b> otherwise.
	 */
	private boolean cp(String fromPath, String toPath, FileServer fromServer, FileServer toServer) {
		
		try{
			byte[] file = null;
			
			// File comes from client
			if(fromServer == null){
				
				File f = new File(basePath,fromPath);
				if(f.exists()&&f.isFile()){
					RandomAccessFile raf = new RandomAccessFile(f, "r");
					file = new byte [(int) f.length()];
					raf.readFully(file);
					raf.close();
				}
			}
			
			// From comes from server
			else
				file = fromServer.downloadFileFromServer(fromPath);
			
			if (file == null)
				return false;
			
			String[] fn = fromPath.split("/");
			String filename = fn [fn.length-1];
			
			// Receive file in client
			if(toServer == null){
				
				Path dir = Paths.get(basePath.toString()+"/"+toPath);
				Path path = dir.resolve(filename);
				Files.createDirectories(path.getParent());
				if(Files.createFile(path) == null)
					return false;
				Files.write(path, file);
				return true;
			}
			
			// Receive file in server
			else
				return toServer.uploadFileToServer(file, filename, toPath);
		
		}catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * Renames a normal file (not directory).
	 * @param filepath - Path to the file.
	 * @param newname - The new name or new string to be appended to the current name.
	 * @param append - If <b>newname</b> is to be appended to the filename.
	 * @return If the operation was sucessful.
	 */
	private boolean rename(String filepath, String newname, boolean append) {
		
		String[] fn = filepath.split("/");
		String parentpath = "";
		byte[] data = null;
		File oldfile = new File(basePath, filepath);
		
		if(oldfile.exists() && oldfile.isFile()){
			try {
				RandomAccessFile raf = new RandomAccessFile(oldfile, "r");
				data = new byte [(int) oldfile.length()];
				raf.readFully(data);
				raf.close();
			} catch (Exception e) {
				System.out.println("Rename operation failed.");
				return false;
			}
		}
		
		else
			return false;
		
		for(int i = 0; i < fn.length - 1; i++) {
			parentpath += fn[i];
			
			if(i < fn.length - 1)
				parentpath += "/";
		}
		
		Path newfilepath = Paths.get(basePath.toString() + "/" + parentpath);
		
		if(append) {
			String filename = fn [fn.length-1];
			newfilepath = newfilepath.resolve(filename + newname);
		}
		else
			newfilepath = newfilepath.resolve(newname);		
		
		try {
			Files.write(newfilepath, data);
		} catch (IOException e) {
			System.out.println("Rename operation failed.");
			return false;
		}		
		
		return true;
	}
	
	/**
	 * Loads the meta-data from .sync directory.
	 */
	@SuppressWarnings("unchecked")
	private boolean loadMeta() {
		
		File syncdir = new File(basePath, SYNC_DIR_PATH);
		
		if(!syncdir.exists() || syncdir.list().length == 0)
			return false;
		
		// .sync not empty
		if(syncdir.list().length != 0) {
			
			File readFile = new File(basePath, SYNC_LAST_MOD_CLIENT);
			
			if(readFile.exists()) {
				try {
					FileInputStream in = new FileInputStream(readFile);
					ObjectInputStream clientOs = new ObjectInputStream(in);
					lastModifiedClient = (HashMap<String, Date>) clientOs.readObject();
					clientOs.close();
					in.close();
				} catch (Exception e) {
					System.out.println("UpdateMeta: Error reading .sync/client");
					readFile.delete(); //TODO: Instead, recursively remove .sync contents
					return false;
				}
			}
			
			readFile = new File(basePath, SYNC_LAST_MOD_SERVER);
			
			if(readFile.exists()) {
				try {
					FileInputStream in = new FileInputStream(readFile);
					ObjectInputStream serverOs = new ObjectInputStream(in);
					lastModifiedServer = (HashMap<String, Date>) serverOs.readObject();
					serverOs.close();
					in.close();
				} catch (Exception e) {
					System.out.println("UpdateMeta: Error reading .sync/server");
					readFile.delete(); //TODO: Instead, recursively remove .sync contents
					return false;
				}
			}
			return true;
		}
		
		else
			return false;
	}
	
	/**
	 * Saves the sync meta-data into .sync folder.
	 * @return If the operation was sucessful.
	 */
	private boolean saveMeta() {
		
		File syncdir = new File(basePath, SYNC_DIR_PATH);
		File clientFile = new File(basePath, SYNC_LAST_MOD_CLIENT);
		File serverFile = new File(basePath, SYNC_LAST_MOD_SERVER);
		
		if(!syncdir.exists())
			syncdir.mkdir();
		
		try {
			if(!clientFile.exists())
				clientFile.createNewFile();
			
			if(!serverFile.exists())
				serverFile.createNewFile();
			
			FileOutputStream fileout = new FileOutputStream(clientFile, false);
			ObjectOutputStream out = new ObjectOutputStream(fileout);
			out.writeObject(lastModifiedClient);
			
			fileout = new FileOutputStream(serverFile, false);
			out = new ObjectOutputStream(fileout);
			out.writeObject(lastModifiedServer);
			out.close();
			fileout.close();
		} catch (Exception e) {
			System.out.println("SaveMeta: Error saving meta into .sync folder");
			e.printStackTrace(System.err);
			return false;
		}
		
		return true;
	}
	
	protected void doit() throws Exception {
		BufferedReader reader = new BufferedReader( new InputStreamReader( System.in));
		
		for( ; ; ) {
			
			System.out.print("> ");
			String line = reader.readLine();
			if( line == null)
				break;
			
			String[] cmd = line.split(" ");
			
			if( cmd[0].equalsIgnoreCase("servers")) { //listar servidores registados no contact server
				if(cmd.length > 2){
					System.out.println("Usage: servers");
					System.out.println("Usage: servers servername");
				}
				else{
					String[] s = servers( cmd.length == 1 ? null : cmd[1]);
					
					if( s == null)
						System.out.println(0); //nao ha servidores registados
					else {
						System.out.println( s.length); //ha s.lenght nomes diferentes de servidores
						for( int i = 0; i < s.length; i++)
							System.out.println( s[i]);
					}
				}	
			} else if( cmd[0].equalsIgnoreCase("ls")) { //detalhar o conteudo de uma directoria do servidor
				if(cmd.length != 2)
					System.out.println("Usage: ls dir");
				else{	
					String[] dirserver = cmd[1].split("@");
					String server = dirserver.length == 1 ? null : dirserver[0]; //se nao tiver havido divisao, o que foi passado eh um url
					boolean isURL = dirserver.length == 1 ? false : dirserver[0].indexOf('/') >= 0; //se nao tiver @ e nao comecar por / entao nao eh um URL
					String dir = dirserver.length == 1 ? dirserver[0] : dirserver[1];
					
					String[] res = dir( server, isURL, dir);
					
					if( res != null) {
						System.out.println( res.length);
						for( int i = 0; i < res.length; i++)
							System.out.println( res[i]);
						
					} else //res == null, nao ha server ou este nao tem essa directoria
						System.out.println( "error"); 
				}	
			} else if( cmd[0].equalsIgnoreCase("mkdir")) { // criar directoria num servidor
				if(cmd.length != 2)
					System.out.println("Usage: mkdir dir");
				else{
					String[] dirserver = cmd[1].split("@");
					String server = dirserver.length == 1 ? null : dirserver[0];
					boolean isURL = dirserver.length == 1 ? false : dirserver[0].indexOf('/') >= 0;
					String dir = dirserver.length == 1 ? dirserver[0] : dirserver[1];
	
					boolean b = mkdir( server, isURL, dir);
					if( b)
						System.out.println( "success");
					else //mkdir falhou: servidor nao existe, ou directoria existe, ou existe ficheiro com esse nome
						System.out.println( "error");
				}
			} else if( cmd[0].equalsIgnoreCase("rmdir")) { //apagar directoria vazia de um servidor
				if(cmd.length != 2)
					System.out.println("Usage: rmdir dir");
				else{
					String[] dirserver = cmd[1].split("@");
					String server = dirserver.length == 1 ? null : dirserver[0];
					boolean isURL = dirserver.length == 1 ? false : dirserver[0].indexOf('/') >= 0;
					String dir = dirserver.length == 1 ? dirserver[0] : dirserver[1];
	
					boolean b = rmdir( server, isURL, dir);
					if( b)
						System.out.println( "success");
					else //rmdir falhou: servidor nao exite, ou directoria nao exite, ou directoria nao esta vazia, ou eh um ficheiro
						System.out.println( "error");
				}	
			} else if( cmd[0].equalsIgnoreCase("rm")) { //apagar um ficheiro de um servidor
				if(cmd.length != 2)
					System.out.println("Usage: rm file");
				else{
					String[] dirserver = cmd[1].split("@");
					String server = dirserver.length == 1 ? null : dirserver[0];
					boolean isURL = dirserver.length == 1 ? false : dirserver[0].indexOf('/') >= 0;
					String path = dirserver.length == 1 ? dirserver[0] : dirserver[1];
	
					boolean b = rm( server, isURL, path);
					if( b)
						System.out.println( "success");
					else //servidor nao exite, ficheiro nao existe, ficheiro eh na realidade directoria, nao teve permissoes para apagar..
						System.out.println( "error");
				}	
			} else if( cmd[0].equalsIgnoreCase("getattr")) { //listar propriedades de um ficheiro ou directoria de um servidor
				if(cmd.length != 2)
					System.out.println("Usage: getattr dir");
				else{
					String[] dirserver = cmd[1].split("@");
					String server = dirserver.length == 1 ? null : dirserver[0];
					boolean isURL = dirserver.length == 1 ? false : dirserver[0].indexOf('/') >= 0;
					String path = dirserver.length == 1 ? dirserver[0] : dirserver[1];
	
					FileInfo info = getAttr( server, isURL, path);
					if( info != null) {
						System.out.println( info);
						System.out.println( "success");
					} else //servidor nao exite, directoria ou ficheiro nao existe, nao ha info
						System.out.println( "error");
				}
			} else if( cmd[0].equalsIgnoreCase("cp")) { //copia um ficheiro entre duas localizacoes - sejam elas servidores de ficheiros ou directorias locais
				if(cmd.length != 3)
					System.out.println("Usage: cp file dir");
				else{
					String[] dirserver1 = cmd[1].split("@");
					String server1 = dirserver1.length == 1 ? null : dirserver1[0];
					boolean isURL1 = dirserver1.length == 1 ? false : dirserver1[0].indexOf('/') >= 0;
					String path1 = dirserver1.length == 1 ? dirserver1[0] : dirserver1[1];
	
					String[] dirserver2 = cmd[2].split("@");
					String server2 = dirserver2.length == 1 ? null : dirserver2[0];
					boolean isURL2 = dirserver2.length == 1 ? false : dirserver2[0].indexOf('/') >= 0;
					String path2 = dirserver2.length == 1 ? dirserver2[0] : dirserver2[1];
	
					boolean b = cp( server1, isURL1, path1, server2, isURL2, path2);
					if( b)
						System.out.println( "success");
					else //muitas razoes possiveis para cp falhar: o servidor/directoria de origem nao existe, o servidor/directoria de destino nao existe, o ficheiro nao existe
						 //o ficheiro indicado eh uma pasta, nao teve permissoes para ler/criar o ficheiro, ja existe ficheiro/pasta com esse nome no destino...
						System.out.println( "error");
				}	
			} else if( cmd[0].equalsIgnoreCase("sync")) {
				
				if(cmd.length != 3)
					System.out.println("Usage: sync dir server@dir");
				
				else {
					String localdir = cmd[1];
					String[] serverdir = cmd[2].split("@");
					
					if(serverdir.length != 2) {
						System.out.println("Usage: sync dir server@dir");
					}
					
					else {
						boolean isUrl = serverdir[0].indexOf('/') >= 0;
						String server = serverdir[0];
						
						if(sync(localdir, server, isUrl, serverdir[1]))
							System.out.println("success");
						else
							System.out.println("error");
					}
				}
				
			} else if( cmd[0].equalsIgnoreCase("help")) { //lista os comandos aceites pelo programa
				System.out.println("servers - lista nomes de servidores a executar");
				System.out.println("servers nome - lista URL dos servidores com nome nome");
				System.out.println("ls server@dir - lista ficheiros/directorias presentes na directoria dir (. e .. tem o significado habitual), caso existam ficheiros com o mesmo nome devem ser apresentados como nome@server;");
				System.out.println("mkdir server@dir - cria a directoria dir no servidor server");
				System.out.println("rmdir server@udir - remove a directoria dir no servidor server");
				System.out.println("cp path1 path2 - copia o ficheiro path1 para path2; quando path representa um ficheiro num servidor deve ter a forma server:path, quando representa um ficheiro local deve ter a forma path");
				System.out.println("rm path - remove o ficheiro path");
				System.out.println("getattr path - apresenta informacao sobre o ficheiro/directoria path, incluindo: nome, boolean indicando se e ficheiro, data da criacao, data da ultima modificacao");
				System.out.println("sync folderdir1 server@folderdir2 - sincroniza a directoria folderdir1 do cliente com a directoria folderdir2 do servidor server.");
			
			} else if( cmd[0].equalsIgnoreCase("exit")) //termina a execucao do programa
				break;

		}
	}
	
	/**
	 * Obtains the RMI reference (stub) to the given server in the URL format or just it's name.
	 * @param server - The server URL or the server name.
	 * @param isURL - Specify if <b>server</b> is a URL or not.
	 * @param primary - Whether is the server to obtain must be primary or not.
	 * @return The RMI reference to the server (stub), {@link FileServer}. Or <i>null</i> if the such server could not be provided.
	 * @throws RemoteException When server is unreachable.
	 * @throws NotBoundException  When server is not bound to RMI register.
	 * @throws MalformedURLException When the given URL is not valid.
	 */
	private FileServer getServerStub(String server, boolean isURL, boolean primary) {
		//TODO: Replace repeated code with this method
		if(isURL) {
			
			try {
				if(primary && !cs.isPrimary(server)) {
					System.out.println("getServerStub: " + server + " is not primary.");
					return null;
				}
			} catch (RemoteException e) {
				System.out.println("getServerStub: ContactServer unreachable.");
			}
			
			try {
				return (FileServer) Naming.lookup(server);
			} catch (RemoteException e) {
				System.out.println("getServerStub: " + server + " seems unreachable.");
				return null;
			} catch (MalformedURLException e) {
				System.out.println("getServerStub: " + server + " is not a valid URL.");
				return null;
			} catch (NotBoundException e) {
				System.out.println("getServerStub: " + server + " is not bound.");
				return null;
			}
		}
		else {
			
			if(primary) {
				String serverURL = "Primary of " + server;
				try {
					serverURL = cs.getPrimaryServer(server);
					return (FileServer) Naming.lookup(serverURL);
				} catch (RemoteException e) {
					System.out.println("getServerStub: " + serverURL + " seems unreachable.");
					return null;
				} catch (MalformedURLException e) {
					System.out.println("getServerStub: " + serverURL + " is not a valid URL.");
					return null;
				} catch (NotBoundException e) {
					System.out.println("getServerStub: " + serverURL + " is not bound.");
					return null;
				}
			}
			
			String[] servs;
			try {
				servs = cs.listServers(server);
			} catch (RemoteException e1) {
				System.out.println("getServerStub: ContactServer unreachable.");
				return null;
			}
			int i = 0;
			while(i<servs.length){
				try{
					return (FileServer) Naming.lookup(servs[i]);
				}catch(Exception e){
					i++;
				}
			}
		}
		return null;
	}
	
	private static ContactServer seekContactServer(String csURL) throws IOException, NotBoundException {

		ContactServer cs = null;
		String csAddr = "//localhost/" + ContactServer.CONTACT_SERVER_NAME;
		int sentPackets = 0;
		boolean keepSending = true;
			
		DatagramSocket responseSocket = new DatagramSocket();
		
		DatagramPacket connection_request;
		InetAddress csadr = null;
		if(csURL != null){
			String s = csURL.substring(csURL.indexOf("/")+2, csURL.lastIndexOf("/"));
			try{
				cs = (ContactServer) Naming.lookup(csURL);
				csadr =InetAddress.getByName(s);
				connection_request = new DatagramPacket(new byte[0], 0, csadr, ContactServer.MULTICAST_SERVER_PORT);
				responseSocket.send(connection_request);
				responseSocket.close();
				System.out.println("Contact server found in "+csURL);
				return cs;
			}catch (Exception e){
				System.out.println("No contact server found at "+csURL+" .\nSearching the local network for a contact server.");
			}
			
		}
		
		InetAddress group = InetAddress.getByName(ContactServer.MULTICAST_GROUP);				
		byte[] buf = new byte[1024];
		DatagramPacket rcv = new DatagramPacket(buf, buf.length);
		responseSocket.setSoTimeout(4000);

		connection_request = new DatagramPacket(new byte[0], 0, group, ContactServer.MULTICAST_SERVER_PORT);

		while (keepSending) {
			
			try {
				responseSocket.send(connection_request);
				sentPackets++;
				System.out.println("Sent packet " + sentPackets);
				responseSocket.receive(rcv);
				csAddr = new String(rcv.getData(), 0, rcv.getLength());
				System.out.println("Received \"" + csAddr + "\" from " + rcv.getAddress() + ":" + rcv.getPort());
				cs = (ContactServer) Naming.lookup(csAddr);
				keepSending = false;
				responseSocket.close();
				System.out.println("Contact server found on " + rcv.getAddress() + ":" + rcv.getPort());
				
			} catch (SocketTimeoutException e2) {
				// keep sending
			} catch (Exception e) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException ie) {
				ie.printStackTrace();
				}
			}

		}
		
		return cs;
	}
	
}
