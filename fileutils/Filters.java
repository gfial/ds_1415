package fileutils;

import java.io.File;
import java.io.FilenameFilter;

public class Filters {
	
	/**
	 * Provides a filter to use with {@link File#list(FilenameFilter)} that blocks files<br/>
	 * starting with "." (example: .sync)
	 * @return A filenameFilter.
	 */
	public static FilenameFilter getHiddenFilter() {
	
		return new FilenameFilter() {
			
			@Override
			public boolean accept(File file, String filename) {
				
				if(filename.startsWith("."))
					return false;
					
				return true;
			}
		};		
	}
}
