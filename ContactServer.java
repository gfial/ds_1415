

import java.net.MalformedURLException;
import java.rmi.Remote;
import java.rmi.RemoteException;


public interface ContactServer extends Remote {

	// Comunication protocol
	public static final String CONTACT_SERVER_NAME = "ContactServer";
	public static final int MAX_SERVER_NAME_LENGHT = 64;
	public static final int MULTICAST_SERVER_PORT = 5000;
	public static final int MAX_NONRESPONDING_TIME = 5000;
	public static final String MULTICAST_GROUP = "224.10.10.10";
	public static final String FILESERVER_ACK = "ACK";
	public static final String FILESERVER_HEARTBEAT = "<3";
	
	/**
	 * Registers the FileServer using it's name, register name and address in the contact server's known servers
	 * map.
	 * @param name The FileServer name
	 * @param srvUrl The FileServer IPv4 address plus it's unique name, unique in it's 
	 * own registry/machine (e.g. //192.168.20.20/Server_1).
	 * @return true if the arguments given are processable, false if not.
	 * @throws RemoteException
	 * @throws MalformedURLException
	 */
	public boolean registerServer(String name, String srvUrl) throws RemoteException, MalformedURLException;
	
	/**
	 * @return  The port of the contact server to which "stay-alive" heartbeats must be sent.
	 */
	public int getHeartbeatPort() throws RemoteException;
	
	public String[] listServers(String name) throws RemoteException;
	
	/**
	 * Checks if the <b>server</b> URL is a primary one or not.
	 * @param server - The URL of the server.
	 * @return If the server is primary or not.
	 */
	public boolean isPrimary(String server) throws RemoteException;
	
	/**
	 * Returns the primary server for the given name.
	 * @param name - Server name
	 * @return URL of the primary server
	 */
	public String getPrimaryServer(String name) throws RemoteException;
	
	/**
	 * Returns a list of secondary servers under the specified name.
	 * @param name - The name of the secondary servers.
	 * @return A String array with a name of a secondary server in each position.<br/>
	 * Null if no secondary servers exist.
	 */
	public String[] getSecondaryServers(String name) throws RemoteException;
	
}
