import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.scribe.builder.ServiceBuilder;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Response;
import org.scribe.model.Token;
import org.scribe.model.Verb;
import org.scribe.model.Verifier;
import org.scribe.oauth.OAuthService;


public class ProxyDrive extends Server{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final String API_KEY = "825027704080-0rqdmsisale19kqfcd583mt2mr3152tu.apps.googleusercontent.com";
	private static final String API_SECRET = "wMfLOqlFwHYmaXxv4s7ZSu7Q";
//	private static String REDIRECT_URI = "urn:ietf:wg:oauth:2.0:oob";
	private static Token accessToken;
    private static final Token EMPTY_TOKEN = null;
	private static String API_CODE_FILE = "src/driveB";
	private static OAuthService service;
	private static String SCOPE = "https://www.googleapis.com/auth/drive.file https://www.googleapis.com/auth/drive.metadata"; //TODO : acrescentar .file depois
	private static String rootID = "0BzxlIxuylK9zfk1UVDBUWXFsc1lmX2dweWJxb09TaEJGV19uYzNLSmhENlFsVkJEeHY5MWc";
	
	public enum Erro{
		File("Erro no ficheiro."),
		NoFile("Ficheiro nao existe."),
		Iniciar("Erro ao iniciar.");	
		String msg;
		private Erro(String msg){
			this.msg = msg;
		}
		public String toString(){
			return this.msg;		
		}
	};
	
	public enum DriveAuth{
		REQUEST_URL("https://www.googleapis.com/drive/v2/files"), 
		GetPerm("Tem de obter autorizacao para a aplicacao continuar acedendo ao link:"),
		CopyPaste("De seguida colar aqui o codigo gerado"),
		PressEnter("E carregar em enter quando der autorizacao.");
		String msg;
		private DriveAuth(String msg){
			this.msg = msg;
		}
		public String toString(){
			return this.msg;		
		}
	};
	
	public enum DriveReq{
		BASE("https://www.googleapis.com/drive/v2"),
		LIST(BASE+"/files"),
		CREATE_DIR(LIST+""),//para ja sao iguais, mas podem vir a mudar
		REM_DIR(LIST+""),
		UPLOAD_FILE("https://www.googleapis.com/upload/drive/v2/files"),
		PARAM_MEDIA("?uploadType=media");
		private String msg;
		private DriveReq(String msg){
			this.msg = msg;
		}
		public String toString(){
			return this.msg;		
		}
		
	}
	
	protected ProxyDrive(String name, String path, String csaddr) throws IOException, NotBoundException {
		super();
		try{
			//le o ficheiro correspondente para apanhar o access token anterior
			FileInputStream fin = new FileInputStream(API_CODE_FILE);
			ObjectInputStream ois = new ObjectInputStream(fin);
			accessToken = (Token) ois.readObject();
			ois.close();
		}catch(Exception e){
			System.out.println(Erro.File);
		}
		
		Class<Google2Api> prov = Google2Api.class;
		service = new ServiceBuilder().provider(prov).apiKey(API_KEY)
				.apiSecret(API_SECRET).scope(SCOPE).build();
		Response response = null;
		OAuthRequest request = new OAuthRequest(Verb.GET,DriveAuth.REQUEST_URL.toString());
		service.signRequest(accessToken, request); //tenta entrar com o codigo anterior
		response = request.send();
		if (response.getCode() != 200){ // o codigo ja nao eh valido
			connectToService(); //liga a drive, e actualiza o token se necess�rio
		}
		if(name == null)
			name = "gd";
		begin(name, path, csaddr);	
	}

	@Override
	public String[] ls(String path) throws RemoteException,
			InfoNotFoundException {
		if(path.equalsIgnoreCase("."))
			path = rootID;
		else
			path = rootID +"/"+ path;
		try{
			
			String parent = getParentDirID(path,0); //para apanhar o id da pasta desejada
			String URL = DriveReq.LIST.toString()+"/"+parent+"/children";
			OAuthRequest request = new OAuthRequest(Verb.GET,URL);;
			service.signRequest(accessToken, request); //tenta entrar com o codigo anterior
			Response response = request.send();
			if (response.getCode() == 401){ // access token expirado
				connectToService();
				service.signRequest(accessToken, request); //tenta entrar com o novo codigo
				request = new OAuthRequest(Verb.GET,URL);
				response = request.send();
			}
			if ( response.getCode() != 200)  
				return null;

			JSONParser parser = new JSONParser();
			JSONObject res;
			res = (JSONObject) parser.parse(response.getBody());
			JSONArray items = (JSONArray) res.get("items");
			@SuppressWarnings("unchecked")
			Iterator<JSONObject> it = items.iterator();
			String [] aux = new String[50]; //no m�ximo aceitamos 50 directorias/ficheiros por pasta
			int i = 0;
			while (it.hasNext()) { //� necessario este passo porque o google drive nao fornece os nomes das subpastas e ficheiros
									//entao eh preciso ir um a um busca-los
				JSONObject file = it.next();
				aux[i] = file.get("childLink").toString(); 
				response = sendRequest(aux[i]);
				res = (JSONObject) parser.parse(response.getBody());
				aux[i] = res.get("title").toString(); 
				i++;
			}
			String [] result = new String[i];
			for(int j = 0; j < i; j++){
				result[j]  = aux [j];
			}
			
			return result;
	} catch (ParseException e) {
		return null;
	}
	
	}

	private String getParentDirID(String path, int level) {
		String [] dir = path.split("/");
		int i = 0;
		boolean ok = true;
		String parentID = dir[0]; //na primeira iteracao nao � ID, � o alias root
		while(i<dir.length-level-1){
			String URL = DriveReq.LIST.toString()+"/"+parentID+
					"/children?q=title+%3D+'"+dir[i+1]+"'&key="+API_KEY;
			OAuthRequest request = new OAuthRequest(Verb.GET,URL);
			service.signRequest(accessToken, request); //tenta entrar com o codigo anterior
			Response response = request.send();
			if (response.getCode() == 401){ // access token expirado
				connectToService();
				service.signRequest(accessToken, request); //tenta entrar com o novo codigo
				request = new OAuthRequest(Verb.GET,URL);
				response = request.send();
			}
			if ( response.getCode() != 200)  
				return null;

			JSONParser parser = new JSONParser();
			JSONObject res;
			try {
				res = (JSONObject) parser.parse(response.getBody());
				JSONArray items = (JSONArray) res.get("items");
				res = (JSONObject) items.get(0);
				parentID = res.get("id").toString();
				i++;
			} catch (ParseException e) {
				ok  = false;
			}
		}
		if(ok)
			return parentID;
		return null;

	}

	@Override
	public boolean mkdir(String from, String dirname) throws RemoteException,
			InfoNotFoundException, MalformedURLException, NotBoundException {
		if(!isPrimary){    //se nao for primario
			if(!from.equals(primary)) //e o from nao for do primario (foi o cliente directamente a pedir)
				return false; //falha
		}
		
		else{ // eh primario		
			FileServer fs;	
			Iterator<String> it = secondaries.iterator();
			String currSrv;
			while(it.hasNext()) {
				 currSrv = it.next();
				 
				 try {
						fs = (FileServer) Naming.lookup(currSrv);
						if(!fs.mkdir(primary, dirname))
							System.out.println("SECONDARY (" + currSrv + ") refused mkdir.");
					} catch(RemoteException re) {
						System.out.println("SECONDARY (" + currSrv + ") failed with RemoteException.");
						secondaries.remove(currSrv);
					}
			}

		}
		String foldername = dirname.substring(dirname.lastIndexOf("/")+1);
		dirname = rootID+"/"+dirname;
		try{
			String parentID = getParentDirID(dirname, 1);
			String URL = DriveReq.CREATE_DIR.toString();
			String folderAlreadyExists = parentID+"/"+foldername;
			try{
				getParentDirID(folderAlreadyExists, 0); //porque o google drive permite varias pastas com o mesmo nome na mesma directoria
			}catch(Exception e){ //se nao existir cai aqui
				Response response = sendPostRequest(URL,foldername,parentID); //aqui cria realmente a nova pasta
				if(response.getCode() != 200)
					return false;
				return true;
			}
			return false; //a pasta ja existia antes. Nao vai ser criada
		}catch(Exception e){
				return false;
		}
	}

	@Override
	public boolean mkdir(String dirname) throws RemoteException,
			InfoNotFoundException, MalformedURLException, NotBoundException {
		return mkdir("",dirname);
	}
	

	@Override
	public boolean rmdir(String from, String dirname) throws RemoteException,
			InfoNotFoundException, MalformedURLException, NotBoundException {
		if(!isPrimary){    //se nao for primario
			if(!from.equals(primary)) //e o from nao for do primario (foi o cliente directamente a pedir)
				return false; //falha
		}
		
		else{ // eh primario		
			FileServer fs;	
			Iterator<String> it = secondaries.iterator();
			String currSrv;
			while(it.hasNext()) {
				 currSrv = it.next();
				 
				 try {
						fs = (FileServer) Naming.lookup(currSrv);
						if(!fs.rmdir(primary, dirname))
							System.out.println("SECONDARY (" + currSrv + ") refused mkdir.");
					} catch(RemoteException re) {
						System.out.println("SECONDARY (" + currSrv + ") failed with RemoteException.");
						secondaries.remove(currSrv);
					}
			}

		}
		String filename = dirname.substring(dirname.lastIndexOf("/")+1);
		dirname = rootID+"/"+dirname;
		try{
			String parentID = getParentDirID(dirname, 1);
			String folderAlreadyExists = parentID+"/"+filename;
			String folderID = getParentDirID(folderAlreadyExists, 0); 

			String URL = DriveReq.LIST.toString()+"/"+folderID;
			OAuthRequest request = new OAuthRequest(Verb.GET,URL);;
			service.signRequest(accessToken, request); //tenta entrar com o codigo anterior
			Response response = request.send();

			if (response.getCode() == 401){ // access token expirado
				connectToService();
				service.signRequest(accessToken, request); //tenta entrar com o novo codigo
				request = new OAuthRequest(Verb.GET,URL);
				response = request.send();
			}
			if ( response.getCode() != 200)  
				return false;
			JSONParser parser = new JSONParser();
			JSONObject res = (JSONObject) parser.parse(response.getBody());
			String type = res.get("mimeType").toString();
			if(!type.equalsIgnoreCase("application/vnd.google-apps.folder")) //sse for file sai aqui
				return false;
			
			URL = DriveReq.LIST.toString()+"/"+folderID+"/children"; //para saber se esta vazia	
			request = new OAuthRequest(Verb.GET,URL);;
			service.signRequest(accessToken, request); //tenta entrar com o codigo anterior
			response = request.send();
			if (response.getCode() == 401){ // access token expirado
				connectToService();
				service.signRequest(accessToken, request); //tenta entrar com o novo codigo
				request = new OAuthRequest(Verb.GET,URL);
				response = request.send();
			}
			if ( response.getCode() != 200)  
				return false;

			parser = new JSONParser();
			res = (JSONObject) parser.parse(response.getBody());
			JSONArray items = (JSONArray) res.get("items");
			res = (JSONObject) parser.parse(response.getBody());
			if(!items.isEmpty()) //quer dizer que o folder nao tem conteudo
				return false; //para garantir que nao se apagam pastas com conteudos
			
			URL = DriveReq.REM_DIR.toString()+"/"+parentID+"/children/"+folderID;
			response = sendDeleteRequest(URL,filename,parentID); //aqui apaga realmente a pasta
			if(response.getCode() != 200 && response.getCode() != 204)
				return false;
			return true;
		}catch(Exception e){ //se nao existir cai aqui
			return false;
		}
	}

	@Override
	public boolean rmdir(String dirname) throws RemoteException,
			InfoNotFoundException, MalformedURLException, NotBoundException {
		return rmdir("",dirname);
	}

	@Override
	public boolean rm(String from, String path) throws RemoteException,
			InfoNotFoundException, MalformedURLException, NotBoundException {
		if(!isPrimary){    //se nao for primario
			if(!from.equals(primary)) //e o from nao for do primario (foi o cliente directamente a pedir)
				return false; //falha
		}
		
		else{ // eh primario		
			FileServer fs;	
			Iterator<String> it = secondaries.iterator();
			String currSrv;
			while(it.hasNext()) {
				 currSrv = it.next();
				 
				 try {
						fs = (FileServer) Naming.lookup(currSrv);
						if(!fs.rm(primary, path))
							System.out.println("SECONDARY (" + currSrv + ") refused mkdir.");
					} catch(RemoteException re) {
						System.out.println("SECONDARY (" + currSrv + ") failed with RemoteException.");
						secondaries.remove(currSrv);
					}
			}

		}
		String filename = path.substring(path.lastIndexOf("/")+1);
		path = rootID+"/"+path;
		try{
			String parentID = getParentDirID(path, 1);
			
			String file = parentID+"/"+filename;
			
			String fileID = getParentDirID(file, 0); 
			
			String URL = DriveReq.LIST.toString()+"/"+fileID;

			OAuthRequest request = new OAuthRequest(Verb.GET,URL);;
			service.signRequest(accessToken, request); //tenta entrar com o codigo anterior
			Response response = request.send();
			if (response.getCode() == 401){ // access token expirado
				connectToService();
				service.signRequest(accessToken, request); //tenta entrar com o novo codigo
				request = new OAuthRequest(Verb.GET,URL);
				response = request.send();
			}
			if ( response.getCode() != 200)  
				return false;

			JSONParser parser = new JSONParser();
			JSONObject res = (JSONObject) parser.parse(response.getBody());

			String type = res.get("mimeType").toString();
			if(type.equalsIgnoreCase("application/vnd.google-apps.folder"))
				return false;
			URL = DriveReq.REM_DIR.toString()+"/"+fileID;
			response = sendDeleteRequest(URL,filename,parentID); //aqui apaga realmente a pasta
			if(response.getCode() != 200 && response.getCode() != 204)
				return false;
			return true;
		}catch(Exception e){ //se nao existir cai aqui
			return false;
		}
	}

	@Override
	public boolean rm(String path) throws RemoteException,
			InfoNotFoundException, MalformedURLException, NotBoundException {
		return rm("",path);
	}

	@Override
	public FileInfo getattr(String path) throws RemoteException,
			InfoNotFoundException {
		String filename = path.substring(path.lastIndexOf("/")+1);
		path = rootID+"/"+path;
		try{
			String parentID = getParentDirID(path, 1);
			
			String file = parentID+"/"+filename;
			
			String fileID = getParentDirID(file, 0); 
			
			String URL = DriveReq.LIST.toString()+"/"+fileID; 
			Response response = sendRequest(URL);
			if ( response.getCode() != 200)  
				return null;
			JSONParser parser = new JSONParser();
			JSONObject res = (JSONObject) parser.parse(response.getBody());
			long size = 0;
			String isDir = res.get("mimeType").toString();
			boolean isFile ;
			if(isDir.equalsIgnoreCase("application/vnd.google-apps.folder")){
					isFile=false; 
			}else{
				isFile=true;
				size =  Integer.parseInt(res.get("fileSize").toString());
			}
			String modified = res.get("modifiedDate").toString(); //nao pode ser passada directamente por causa do formato
			String name = res.get("title").toString(); 
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			Date mod = df.parse(modified);
			FileInfo fi = new FileInfo(name, size, mod, isFile);
			return fi;	
			}catch(Exception e){ //se nao existir cai aqui
			return null;
		}
	}

	@Override
	public byte[] downloadFileFromServer(String path)
			throws RemoteException, InfoNotFoundException {
		String filename = path.substring(path.lastIndexOf("/")+1);
		path = rootID+"/"+path;
		try{
			String parentID = getParentDirID(path, 1);
			
			String file = parentID+"/"+filename;
			
			String fileID = getParentDirID(file, 0); 
			
			String URL = DriveReq.LIST.toString()+"/"+fileID+"?fields=downloadUrl"; 
			Response response = sendRequest(URL);
			if ( response.getCode() != 200)  
				return null;
			JSONParser parser = new JSONParser();
			JSONObject res = (JSONObject) parser.parse(response.getBody());
			if(res.get("downloadUrl") == null)
				return null;
			String downloadUrl = (String) res.get("downloadUrl");
			response = sendRequest(downloadUrl);
			if ( response.getCode() != 200)  
				return null;
			InputStream is = response.getStream();
			DataInputStream dis = new DataInputStream(is);
			int size = Integer.parseInt(response.getHeader("Content-Length"));
			byte[] filedata = new byte[size];
			dis.readFully(filedata);
			return filedata;
			}catch(Exception e){ //se nao existir cai aqui
			return null;
		}
	}

	@Override
	public boolean uploadFileToServer(String from, byte[] file,
			String filename, String toPath) throws RemoteException,
			InfoNotFoundException, MalformedURLException, NotBoundException {
		if(!isPrimary){    //se nao for primario
			if(!from.equals(primary)) //e o from nao for do primario (foi o cliente directamente a pedir)
				return false; //falha
		}
		
		else{ // eh primario	
			FileServer fs;	
			Iterator<String> it = secondaries.iterator();
			String currSrv;
			while(it.hasNext()) {
				 currSrv = it.next();
				 
				 try {
						fs = (FileServer) Naming.lookup(currSrv);
						if(!fs.uploadFileToServer(primary, file,
								filename, toPath))
							System.out.println("SECONDARY (" + currSrv + ") refused mkdir.");
					} catch(RemoteException re) {
						System.out.println("SECONDARY (" + currSrv + ") failed with RemoteException.");
						secondaries.remove(currSrv);
					}
			}

		}
		
		try{
			String path ="";
			if(toPath.equalsIgnoreCase("."))
				path = rootID;
			else
				path = rootID +"/"+ toPath;
			String parentID = getParentDirID(path, 0);
			String fileurl = parentID+"/"+filename;
			try{
				getParentDirID(fileurl, 0);
				return false; //ja existe ficheiro com esse nome
			}
			catch(Exception e){
				
			}
			String URL = DriveReq.UPLOAD_FILE.toString();
			Response response = sendPostRequest2(URL, filename, parentID, file);
			if(response.getCode()!=200 && response.getCode() != 404)
				return false;
			return true;
		}
		catch(Exception e){
			
		}
		return false;
	}

	@Override
	public boolean uploadFileToServer(byte[] file, String filename,
			String toPath) throws RemoteException, InfoNotFoundException, MalformedURLException, NotBoundException {
		return uploadFileToServer("", file, filename, toPath);
	}
	/**
	 * connect to service tenta ligar a dropbox.s
	 * � usado quando o pedido anterior tiver falhado por token expirado
	 * guarda em memoria e em disco o novo token
	 */
	private static void  connectToService(){

				//Pedir autorizacao
				Verifier verifier = requestManualAuth();
	            
				//Pedir novo token
				accessToken = service.getAccessToken(EMPTY_TOKEN, verifier);
				
				// Guardar token  para uso futuro
				FileOutputStream fout;
				try {
					fout = new FileOutputStream(API_CODE_FILE);
					ObjectOutputStream oos = new ObjectOutputStream(fout);
					oos.writeObject(accessToken);
					oos.close();
				} catch (Exception e) {
					System.out.println(Erro.File);
				}
			}
		
	/**
	 * Pede ao user para manualmente autorizar o acesso da app a drive
	 * @param requestToken � o token que faz o url de acesso especifico
	 * @return o verificador que permite entender se o processo foi bem sucedido
	 */
	private static Verifier requestManualAuth() {
		String authorizationUrl = service.getAuthorizationUrl(EMPTY_TOKEN);
		Scanner in = new Scanner(System.in);
		System.out.println(DriveAuth.GetPerm);
		System.out.println(authorizationUrl);
		System.out.println(DriveAuth.CopyPaste);
		System.out.println(DriveAuth.PressEnter);
        System.out.print(">>");
        Verifier  verifier = new Verifier(in.nextLine());
        in.close();
		System.out.println("Thanks");
		 return verifier;
	}
	
	private Response sendRequest(String URL) {
		OAuthRequest request = new OAuthRequest(Verb.GET,URL);
		service.signRequest(accessToken, request); //tenta entrar com o codigo anterior
		Response response = request.send();
		if (response.getCode() == 401){ // access token expirado
			connectToService();
			service.signRequest(accessToken, request); //tenta entrar com o novo codigo
			request = new OAuthRequest(Verb.GET,URL);
			response = request.send();
		}
		return response;
		
	}

	private Response sendDeleteRequest(String URL, String foldername,
			String parentID) {
		OAuthRequest request = new OAuthRequest(Verb.DELETE,URL);
		service.signRequest(accessToken, request); //tenta entrar com o codigo anterior
		Response response = request.send();
		if (response.getCode() == 401){ // access token expirado
			connectToService();
			service.signRequest(accessToken, request); //tenta entrar com o novo codigo
			request = new OAuthRequest(Verb.DELETE,URL);
			response = request.send();
		}
		return response;
	}
	@SuppressWarnings("unchecked")
	private Response sendPostRequest(String URL,String fname, String parentId) {
		OAuthRequest request = new OAuthRequest(Verb.POST,URL);
		service.signRequest(accessToken, request); //tenta entrar com o codigo anterior
		
		JSONObject bodyParam = new JSONObject();

		request.addHeader("Content-Type","application/json");
		bodyParam.put("mimeType", "application/vnd.google-apps.folder");

		bodyParam.put("title", fname);
		JSONArray parents = new JSONArray();
		JSONObject val = new JSONObject();
		val.put("id", parentId);
		parents.add(val);
		bodyParam.put("parents", parents);
		request.addPayload(bodyParam.toJSONString());
		Response response = request.send();
		if (response.getCode() == 401){ // access token expirado
			connectToService();
			service.signRequest(accessToken, request);
			response = request.send();
		}
		return response;
		
	}
	@SuppressWarnings("unchecked")
	private Response sendPostRequest2(String URL,String fname, String parentId, byte[] file) {
		String URL1 = DriveReq.LIST.toString();
		try{
			OAuthRequest request = new OAuthRequest(Verb.POST,URL1);
			if(file == null)
				return null;
			service.signRequest(accessToken, request); //tenta entrar com o codigo anterior
			JSONObject bodyParam = new JSONObject();
	
			request.addHeader("Content-Type", "application/json");
			bodyParam.put("mimeType", getMimeType(fname));
			bodyParam.put("title", fname);
			JSONArray parents = new JSONArray();
			JSONObject val = new JSONObject();
			val.put("id", parentId);
			parents.add(val);
			bodyParam.put("parents", parents);
			request.addPayload(bodyParam.toJSONString());
		
			Response response = request.send();
			if (response.getCode() == 401){ // access token expirado
				connectToService();
				service.signRequest(accessToken, request);
				response = request.send();
			}
			if(response.getCode()!= 200)
				return null;
			JSONParser parser = new JSONParser();
			JSONObject res = (JSONObject)parser.parse(response.getBody());
			String id = res.get("id").toString();
			URL1 = URL +"/"+id+DriveReq.PARAM_MEDIA;
			request = new OAuthRequest(Verb.POST,URL1);
			request.addHeader("Content-Type", "application/octet-stream");
			request.addHeader("Content-Length",  file.length+"");
			request.addPayload(file);
			response = request.send();
			if (response.getCode() == 401){ // access token expirado
				connectToService();
				service.signRequest(accessToken, request);
				response = request.send();
			}
			return response;
			
		}
		catch(Exception e){
			return null;
		}
		
		
	}
	private String getMimeType(String fname) {
		String mimeType = fname.substring(fname.lastIndexOf(".")+1);
		if(mimeType.equalsIgnoreCase(fname)) //nao tem extensao
			mimeType = "";
		return mimeType;
	}

	public static void main(String[] args) throws IOException, NotBoundException
    {
		getStarted(args);
		try {
			new ProxyDrive(name, path, csaddr);
		} catch (Exception e) {
			System.out.println(Erro.Iniciar);
			System.exit(0);
		}
    }
   
}
